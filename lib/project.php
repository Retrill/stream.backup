<?php
namespace Webformat\StreamBackup;
use \Webformat\StreamBackup\Utils;
use \Webformat\StreamBackup\Ini\Calculator;

Class Project{
    protected $root;
    protected $name;
    protected $taskDir;
    protected $archiveName = '#project_name#-#date#.tar';
    //protected $iterationKey;
    protected $aliases = array();
    protected $parentProject;
    protected $ini;
    protected $remoteStorageRes = array('process' => false, 'pipes' => array());
    //protected $dbDumper;
    
    public function __construct($taskDir, $projectRoot = '', $projectName = 'backup' /*, $iterationKey = false*/){
        $this->root = $projectRoot;
        $this->name = $projectName;
        $this->taskDir = rtrim($taskDir, DIRECTORY_SEPARATOR);
        //$this->iterationKey = $iterationKey;
        if(!(bool)$this->taskDir){$this->taskDir = '/';}
    }
    
    public function setParent(\Webformat\StreamBackup\Project &$project){
        $this->parentProject = $project;
    }
    
    public function execute(){
        $this->resolveProjectName();
        $this->resolveTaskDir();
        $this->resolveArchiveName();
        $this->ini = Ini::parseFromDir($this->taskDir, true);
        if(empty($this->ini[Ini::CONFIG_EXEC])){Utils::report('Empty execute task for project "'.$this->root.'"'); return false;}
        Ini::doExtend($this->ini);

        if($this->iniExecOwn = Ini::parseFromDir($this->taskDir, false, Ini::CONFIG_EXEC)){
            $this->iniExecOwn = $this->iniExecOwn[Ini::CONFIG_EXEC];
        }
        
        $ownTask = empty($this->parentProject) || ($this->taskDir != $this->parentProject->getTaskDir());
   
        if($ownTask && !empty($this->iniExecOwn['project_profile'])){
            //Перебираем итератором и выполняем дочерние проекты
            $projectSections = $this->iniExecOwn['project_profile'];
            if(!is_array($projectSections)){$projectSections = array($projectSections);}
            foreach($projectSections as $iniSectionName){
                if(empty($this->ini[Ini::CONFIG_PROJECTS][$iniSectionName])){
                    Utils::report('Undefined projects profile "'.$iniSectionName.'"');
                }else{
                    $iterator = new \Webformat\StreamBackup\Ini\Iterator\Projects();
                    $iterator->setTaskDir($this->taskDir);
                    $iterator->fill($this->ini[Ini::CONFIG_PROJECTS][$iniSectionName], 'project_root');
    
                    while($project = $iterator->next()){
                        $project->setParent($this);
                        $project->execute();
                    }
                    unset($iterator);
                }
            }        
        }else{
            //Бэкапим текущий проект
            Ini::doReplaces($this->ini, array(
                '#project_root#' => $this->root,
                '#project_name#' => $this->name
            ));

            Ini::doCalculations($this->ini);
            $this->backup();
        }
    }
    public function setArchiveName($archiveName){$this->archiveName = $archiveName;}
    public function setAliases($aliases){
        $this->aliases = array_merge($this->aliases, $aliases);
    }
    
    protected function resolveProjectName(){
        Ini::doReplaces($this->name, array('#project_root#' => $this->root));
        $this->name = Calculator::calc($this->name, $this->root);
        if(isset($this->aliases[$this->name])){
            $this->name = $this->aliases[$this->name];
        }
    }
    protected function resolveArchiveName(){
        Ini::doReplaces($this->archiveName, array(
            '#project_root#' => $this->root,
            '#project_name#' => $this->name,
        ));
        global $logArchive;
        $logArchive = false;
        Ini::doCalculations($this->archiveName);
        $logArchive = false;
    }
    protected function resolveTaskDir(){
        if(!is_dir($this->taskDir)){Utils::report('Task dir "'.$this->taskDir.'" doesn\'t exist!');}
        if(empty($this->name)){return;}
        $projectSubdir = $this->taskDir.DIRECTORY_SEPARATOR.$this->name;
        if(is_dir($projectSubdir)){
            $this->taskDir = $projectSubdir;
        }
    }
    protected function backup(){
        Utils::report('Backuping project name="'.$this->name.'", root="'.$this->root.'", archiveName="'.$this->archiveName.'"');

        if(!$remoteStorageProcId = $this->initRemoteStorageStream($this->ini, $this->archiveName, $remoteStorage)){return false;}
        //Utils::report('Remote storage initialized!');

        //return;
        if(!$encryptor = Encryption\Fabric::get($this->ini)){return false;}
        $encryptionCmd = $encryptor->getCmd();       
        
        if($compressor = Compression\Fabric::get($this->ini)){
            $compressionCmd = $compressor->getCmd();
        }else{
            $compressionCmd = '';
        }

        $encCompProcId = false;
        if($cmd = array_filter(array($compressionCmd, $encryptionCmd))){
            $cmd = implode(' | ', $cmd);
            $encCompProcId = proc_open($cmd, array(0 => array('pipe', 'r'), 1 => $remoteStorage, 2 => \STDERR), $encCompPipes);
        }
        
        $dbDumper = new DbDumper($this, $encCompProcId ? $encCompPipes[0] : $remoteStorage);
        $dbDumper();
        $filesDumper = new FileDumper($this, $encCompProcId ? $encCompPipes[0] : $remoteStorage);
        $filesDumper();
        
        if($encCompProcId){
            foreach($encCompPipes as $descriptor => $resource){
                fclose($descriptor);
            }
            proc_close($encCompProcId);
        }
        $this->closeRemoteStorageStream();
    }
    
    protected function initRemoteStorageStream(&$ini, $filename, &$streamId){
        static $helper, $streamInteractor;
        if(!isset($helper)){$helper = new \Webformat\StreamBackup\Storage\Helper();}

        if(!$profileGroups = $helper->getProfiles($ini)){return false;} //Профили разбиваются на группы
        
        $divergeMode = count($profileGroups) > 1; //Если >1 группы, то нужно отправлять данные в неск-ко процессов
        
        $storageParams = array(
            'filename' => $filename,
            'profiles' => $divergeMode ? $profileGroups : current($profileGroups)
        );
        $cmd = 'php -d mbstring.func_overload=0 '.Utils::getBasedir().'bin/' . ($divergeMode ? 'diverge_output.php' : 'remote_storage.php');

        $procId = proc_open($cmd, array(
        	0 => array('pipe', 'r'), 
        	1 => array('file', '/dev/null', 'w'), 
        	2 => \STDERR, 
        	3 => array('pipe', 'r') //Сюда мы будем передавать скрипту параметры
        ), $pipes);

        $streamId = $pipes[0];
        
        if(!isset($streamInteractor)){
        	$streamInteractor = new \Webformat\StreamBackup\StreamInteractor(false, $pipes[3]);
        }else{
			$streamInteractor->setOutputStream($pipes[3]);
        }
        $streamInteractor->write($storageParams);
        fclose($pipes[3]);
        $this->remoteStorageRes['process'] = $procId;
        $this->remoteStorageRes['pipes'] = $pipes;
        return $procId;
    }
    
    protected function closeRemoteStorageStream(){
		fclose($this->remoteStorageRes['pipes'][0]);
		fclose($this->remoteStorageRes['pipes'][1]);
		fclose($this->remoteStorageRes['pipes'][2]);
		proc_close($this->remoteStorageRes['process']);
    }

    public function getTaskDir(){
        return $this->taskDir;
    }
    public function getIni(){return $this->ini;}
    
    public function getProjectRoot(){
        return $this->root;
    }
}