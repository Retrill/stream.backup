<?php
namespace Webformat\StreamBackup;

Class Options{
	public static function get(){
        $optCodes = array(
            't' => 'task',
        );
        $emptyOpts = array( 
        );

        $options = array_map(function(){return array();}, array_flip($optCodes));

        $shellOpts = getopt(
            implode(':', array_keys($optCodes)).':'.implode('', $emptyOpts), 
            array_merge(array_map(function($paramCode){return $paramCode.':';}, array_values($optCodes)), array_values($emptyOpts))
        );

        foreach($shellOpts as $propCode => $data){
            $propCode = isset($optCodes[$propCode]) ? $optCodes[$propCode] : $propCode;
            $propCode = isset($emptyOpts[$propCode]) ? $emptyOpts[$propCode] : $propCode;

            if(is_array($data)){
                $options[$propCode] = array_merge($options[$propCode], $data);
            }else{
                $options[$propCode][] = $data;
            }
        }
        return $options;
    }
}