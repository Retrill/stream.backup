<?php
namespace Webformat\StreamBackup\Gateway\File;
use Webformat\StreamBackup\Utils;

Class Controller{
    protected $profile;
    protected $fileName;
    
    public function __construct($profile, $fileName){
        $this->profile = $profile;
        $this->fileName = $fileName;
    }
    public function check(){
        if(empty($this->profile['destination'])){Utils::report('Empty destination variable!'); return false;}
        return true;
    }
    public function getCmd(){
        /*$default = array(
            'maxSize' => '0',
            'padLeft' => '3',
        );
        $profile = $default + $this->profile;*/
        
        $fileName = rtrim($this->profile['destination'], '/').'/'.ltrim($this->fileName, '/');
        
        $cmd = 'php -d mbstring.func_overload=0';
        $cmd .= ' '.Utils::getBasedir().'bin'.DIRECTORY_SEPARATOR.'ftp.php';
        $cmd .= ' -f"'.$fileName.'"';
        $cmd .= ' -l"'.$this->profile['maxSize'].'"';
        $cmd .= ' -P"'.$this->profile['padLeft'].'"';
        return $cmd;
    }
}