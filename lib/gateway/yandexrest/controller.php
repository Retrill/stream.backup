<?php
namespace Webformat\StreamBackup\Gateway\YandexRest;
use Webformat\StreamBackup\Utils;

Class Controller{
    protected $profile;
    protected $fileName;
    
    public function __construct($profile, $fileName){
        $this->profile = $profile;
        $this->fileName = $fileName;
    }
    public function check(){
        if(empty($this->profile['host'])){Utils::report('Empty host variable!'); return false;}
        if(!array_key_exists('user', $this->profile)){Utils::report('User variable is not set!'); return false;}
        if(empty($this->profile['user'])){Utils::report('Empty user variable for db-profile "'.$this->profileName.'"!');}
        if(!array_key_exists('password', $this->profile)){Utils::report('Password variable for db-profile "'.$this->profileName.'" is not set!');}
        
        return true;
    }
    public function getCmd(){
        $default = array(
            'dir' => '/',
            'maxSize' => '0',
            'padLeft' => '3',
        );
        $profile = $default + $this->profile;
        $fileName = rtrim($profile['dir'], '/').'/'.ltrim($this->fileName, '/');
        
        $cmd = 'php -d mbstring.func_overload=0';
        $cmd .= ' '.Utils::getBasedir().'bin'.DIRECTORY_SEPARATOR.'ftp.php';
        $cmd .= ' -h"'.$profile['host'].'"';
        $cmd .= ' -u"'.$profile['user'].'"';
        $cmd .= ' -p"'.$profile['password'].'"';
        $cmd .= ' -f"'.$fileName.'"';
        $cmd .= ' -l"'.$profile['maxSize'].'"';
        $cmd .= ' -P"'.$profile['padLeft'].'"';
        return $cmd;
    }
}