<?php
namespace Webformat\StreamBackup\Gateway;
use Webformat\StreamBackup\Utils;
use Webformat\StreamBackup\INI;

Class Fabric{
	public static function get(&$ini, $fileName){
		if(empty($ini[INI::CONFIG_EXEC]['output'])){
			Utils::report('No output profileas signed! Project will be ignored.');
			return false;
		}
		
		$outputProfiles = $ini[INI::CONFIG_EXEC]['output'];
		if(!is_array($outputProfiles)){$outputProfiles = array($outputProfiles);}
		if(count($outputProfiles) > 1){
			Utils::report('Several output profiles detected. This version of backup script not supports this function. First profile will be used only.');
		}
		$outputProfile = current($outputProfiles);
		if(is_array($outputProfile)){
			Utils::report('Error: output profile is array. Use only string value. Project will be ignored.');
			return false;
		}
		if(empty($ini[INI::CONFIG_OUTPUT][$outputProfile])){
			Utils::report('Output profile "'.$outputProfile.'" not found or empty. Project will be ignored.');
			return false;
		}
        
        $outputProfile = $ini[INI::CONFIG_OUTPUT][$outputProfile];
		/*if(strtolower($outputProfile['type']) != 'ftp'){
			Utils::report('Only ftp output profiles are supported, "'.$outputProfile['type'].'" given. Project will be ignored.');
			return false;
		}*/
		
		$controllerClassName = 'Webformat\\StreamBackup\\Gateway\\'.$outputProfile['type'].'\\Controller';
		if(!class_exists($controllerClassName)){
			Utils::report('Controller class for gateway "'.$outputProfile['type'].'" does not exist! Project will be ignored.');
			return false;
		}
		$gateway = new $controllerClassName($outputProfile, $fileName);
		if(!$gateway->check()){
			Utils::report('Gateway "'.$outputProfile['type'].'" check failed. Project will be ignored.');
			return false;
		}
		return $gateway;
	}
}