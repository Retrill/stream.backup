<?php
namespace Webformat\StreamBackup\Compression\GZip;
use Webformat\StreamBackup\Utils;

Class Controller{
    protected $profile;

    public function __construct($profile){
        $this->profile = $profile;
    }
    public function check(){
        return true;
    }
    public function getCmd(){
        $enabled = (bool)(int)$this->profile['enabled'];
        if(!$enabled){return '';}
        
        $default = array(
            'level' => 6,
        );
        $profile = $default + $this->profile;
        $cmd = 'gzip -'.$profile['level'].' -c';
        return $cmd;
    }
}