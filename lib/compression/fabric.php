<?php
namespace Webformat\StreamBackup\Compression;
use Webformat\StreamBackup\Utils;
use Webformat\StreamBackup\INI;

Class Fabric{
    public static function get(&$ini, $profileName = false){
        if(!$profileName){
            if(empty($ini[INI::CONFIG_EXEC]['compression_profile'])){
                Utils::report('No compression profile assigned! Data will not be compressed.');
                return false;
            }
            $profileName = $ini[INI::CONFIG_EXEC]['compression_profile'];
        }
        if(empty($ini[INI::CONFIG_COMPRESSION][$profileName])){
            Utils::report('Compression profile "'.$profileName.'" not found! Data will not be compressed.');
            return false;
        }
        
        $profile = $ini[INI::CONFIG_COMPRESSION][$profileName];
        $profile = array('algorithm' => 'gzip', 'level' => 6) + $profile;

        $controllerClassName = 'Webformat\\StreamBackup\\Compression\\'.$profile['algorithm'].'\\Controller';
        if(!class_exists($controllerClassName)){
            Utils::report('Controller class for compression "'.$profile['algorithm'].'" does not exist! Data will not be compressed.');
            return false;
        }
        $controller = new $controllerClassName($profile, $fileName);
        if(!$controller->check()){
            Utils::report('Compression profile check failed. Data will not be compressed');
            return false;
        }
        return $controller;
    }
}