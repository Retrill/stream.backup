<?php
namespace Webformat\StreamBackup\Encryption\OpenSSL;
use Webformat\StreamBackup\Utils;

Class Controller{
    protected $profile;

    public function __construct($profile){
        $this->profile = $profile;
    }
    public function check(){
        return true;
    }
    public function getCmd(){
        $enabled = (bool)(int)$this->profile['enabled'];
        if(!$enabled){return '';}
        $cmd = 'openssl enc -e -aes-256-cbc -k '.$this->profile['key'];
        return $cmd;
    }
}