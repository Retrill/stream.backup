<?php
namespace Webformat\StreamBackup\Encryption;
use Webformat\StreamBackup\Utils;
use Webformat\StreamBackup\INI;

Class Fabric{
    public static function get(&$ini){
        if(empty($ini[INI::CONFIG_GENERAL]['encryption'])){
            Utils::report('No encryption profile assigned! Project will be ignored.');
            return false;
        }
        
        $profile = $ini[INI::CONFIG_GENERAL]['encryption'];
        $profile = array('algorithm' => 'openssl') + $profile;

        $controllerClassName = 'Webformat\\StreamBackup\\Encryption\\'.$profile['algorithm'].'\\Controller';
        if(!class_exists($controllerClassName)){
            Utils::report('Controller class for encryption "'.$profile['algorithm'].'" does not exist! Project will be ignored.');
            return false;
        }
        $controller = new $controllerClassName($profile, $fileName);
        if(!$controller->check()){
            Utils::report('Encryption profile check failed. Project will be ignored.');
            return false;
        }
        return $controller;
    }
}