<?php
namespace Webformat\StreamBackup;
use \Webformat\StreamBackup\Utils;

Class DbDumper{
    protected $project;
    protected $ini;
    protected $outputProcess;
    
    public function __construct(Project $project, $outputProcess){
        $this->project = $project;
        $this->ini = $project->getIni();
        $this->outputProcess = $outputProcess;
    }
    public function __invoke(){
        if(empty($this->ini[Ini::CONFIG_EXEC]['db_profile'])){
            Utils::report('No database selected to backup');
            return true;
        }
        if(!is_array($this->ini[Ini::CONFIG_EXEC]['db_profile'])){
            Utils::report('"db_profile" ini variable should be an array!');
            return false;
        }
        
        foreach($this->ini[Ini::CONFIG_EXEC]['db_profile'] as $profileName){
            $this->applyProfile($profileName);
        }
    }
    
    protected function applyProfile($profileName){
        if(!array_key_exists($profileName, $this->ini[Ini::CONFIG_DBPROFILES]) || !is_array($this->ini[Ini::CONFIG_DBPROFILES])){
            Utils::report('Database profile "'.$profileName.'" does not exist!');
            return false;
        }
        $profile = $this->ini[Ini::CONFIG_DBPROFILES][$profileName];
        
        if(empty($profile['host'])){Utils::report('Empty host variable for db-profile "'.$profileName.'"!');}
        if(empty($profile['user'])){Utils::report('Empty user variable for db-profile "'.$profileName.'"!');}
        if(!array_key_exists('password', $profile)){Utils::report('Password variable for db-profile "'.$profileName.'" is not set!');}
        if(!$this->testConnection($profile)){
            Utils::report('Test connection '.$profile['user'].'@'.$profile['host'].' failed! Check user/password/host variables.');
            return false;
        }
        if(!$dbList = $this->getDbList($profile)){
            Utils::report('Database profile "'.$profileName.'": no databases!');
            return false;
        }
        
        $isDbDedicated = stripos($profile['to_file'], '#dbname#') !== false;
        if(!$isDbDedicated){$outputProc = $this->getProfileOutputProc($profile, $stdIn, false);}
        
        foreach($dbList as $database){
            if($isDbDedicated){$outputProc = $this->getProfileOutputProc($profile, $stdIn, $database);}
            $this->dumpDatabase($database, $profile, $stdIn);
            if($isDbDedicated){proc_close($outputProc);}
        }
        
        if(!$isDbDedicated){proc_close($outputProc);}
        //echo "show databases;" | mysql -u user -ppassword
        //mysql --user=root --password="$(cat /root/.mysql)"
        //mysql --user=root --password="$(< /root/.mysql)
    }
    protected function getProfileOutputProc(&$profile, &$stdIn, $dbname){
        $cmd = array();
        $removeDefinerCmd = 'sed -e "s#\/\*\![[:digit:]]*\s*DEFINER.*\*\/##"';
        $removeDefinerCmd .= ' | sed -e "s#DEFINER\s*=\s*[^*]*\(FUNCTION\|PROCEDURE\)#\1#"';
        $removeAutoIncCmd = 'sed "s/\s*AUTO_INCREMENT=[0-9]*//g"';
        
        $toFile = $dbname ? str_ireplace('#dbname#', $dbname, $profile['to_file']) : $profile['to_file'];
        
        //Лимит памяти для tar-скрипта будет равен размеру буфера (для хранения партии дампа) + 256 Мб на собственно исполнение php-процесса
        $memoryLimit = Utils::getCanonicalBytes($profile['memory_buffer']) + Utils::getCanonicalBytes('256M');
        $memoryLimit = Utils::formatBytes($memoryLimit, 'M');
        $tarCmd = 'php -d mbstring.func_overload=0 -d memory_limit='.$memoryLimit.' '.Utils::getBasedir().'bin/tar.php -n -m"'.$profile['memory_buffer'].'" -p"#stdin#:'.$toFile.'"';
        $useCompression = !empty($profile['compression_profile']) && $profile['compression_profile'];
        if($useCompression && ($compressor = Compression\Fabric::get($this->ini, $profile['compression_profile']))){
            $compressionCmd = $compressor->getCmd();
        }else{
            $compressionCmd = '';
        }
        
        if((bool)(int)$profile['skip_definer']){
            $cmd[] = $removeDefinerCmd;
        }
        if((bool)(int)$profile['skip_auto_increment']){
            $cmd[] = $removeAutoIncCmd;
        }
        $cmd[] = $compressionCmd;
        $cmd[] = $tarCmd;
        $cmd = implode(' | ', array_filter($cmd));
        Utils::report("Database TAR process: $cmd\n");
        $proc = proc_open($cmd, array(0 => array('pipe', 'r'), 1 => $this->outputProcess), $pipes);
        $stdIn = $pipes[0];
        return $proc;
    }
    protected function getMaxCmdLength(){
        //xargs --show-limits
        $cmd = 'expr `getconf ARG_MAX` - `env|wc -c` - `env|wc -l` \* 4 - 2048'; //see https://www.cyberciti.biz/faq/linux-unix-arg_max-maximum-length-of-arguments/
        
    }
    protected function testConnection(&$profile){
        $cmd = 'mysql -u'.$profile['user'].' -p'.$profile['password'].' -h'.$profile['host'].' -e exit';
        $cmd .= ' 2>/dev/null';
        //exec($cmd, $errors, $status);
        /*$proc = proc_open($cmd, array(2 => array('pipe', 'r')), $pipes);
        $errors = stream_get_contents($pipes[2]);
        fclose($pipes[2]);
        $status = proc_close($proc);*/
        $status = pclose(popen($cmd, 'r'));
        if($status !== 0){return false;}
        return true;
    }
    protected function getDbList(&$profile){
        $cmd = 'echo "show databases;" | mysql -u'.$profile['user'].' -p'.$profile['password'].' -h'.$profile['host'].'';
        $sourceDbList = array();
        exec($cmd, $sourceDbList, $status);
        
        unset($sourceDbList[0]); //remove result column name "Database"
        if(empty($sourceDbList)){return array();}
        
        if(empty($profile['databases'])){
            Utils::report('Empty "databases" variable at db profile!');
            return array();
        }
        
        $outputDbList = array();
        if(!is_array($profile['databases'])){$profile['databases'] = array($profile['databases']);}
        foreach($profile['databases'] as $fnPattern){
            $this->applyDbPattern($fnPattern, $sourceDbList, $outputDbList);
        }
        $outputDbList = array_keys($outputDbList);
        return $outputDbList;
    }
    protected function applyDbPattern($pattern, &$sourceDbList, &$outputDbList){
        $include = true; //include
        if(substr(strtolower($pattern), 0, 2) == 'e:'){
            $include = false; //exclude
            $pattern = substr($pattern, 2);
        }
        if($include){
            foreach($sourceDbList as $dbName){
                if(fnmatch($pattern, $dbName)){
                    $outputDbList[$dbName] = true;
                }
            }
        }else{
            foreach($outputDbList as $dbName => $value){
                if(fnmatch($pattern, $dbName)){
                    $outputDbList[$dbName] = false;
                }
            }
            $outputDbList = array_filter($outputDbList);
        }
    }
    protected function dumpDatabase($dbName, &$profile, &$outputProc){
        static $tableFilter;
        Utils::report('Start to dump database "'.$dbName.'"');
        if(!isset($tableFilter)){
            $tableFilter = new TableFilter();
        }
        $connection = array_intersect_key($profile, array_flip(array('host', 'user', 'password')));
        $connection['database'] = $dbName;
        
        $tableFilter->setConnection($connection);
        $patterns = isset($profile['tables']) ? $profile['tables'] : array();
        $tableFilter->setTablePatterns($patterns);
        
        if(!$r = $tableFilter->getResult()){
            Utils::report('No tables prepared to dump');
            if(empty($profile['mysqldump']['triggers']) && empty($profile['mysqldump']['routines'])){
                Utils::report('No triggers and routines directions to dump. Database will be ignored');
                return false;
            }
        }

        $options = array(
            '-h' => '"'.$connection['host'].'"',
            '-u' => '"'.$connection['user'].'"',
            '-p' => '"'.$connection['password'].'"'
        );
        if(!empty($profile['md'])){
            foreach($profile['md'] as $option => $value){
                $options['--'.$option] = '"'.$value.'"';
            }
        }
        foreach($r as $type => $distribution){
            $cmd = 'mysqldump';
            $cmdArgs = $options;
            if($type == TableFilter::STRUCTURE){
                $cmdArgs = array(
                    '--no-data' => '"true"',
                    '--quick' => '"true"',
                    '--skip-dump-date' => '"true"',
                    '--skip-disable-keys' => '"true"',
                    '--skip-set-charset' => '"true"',
                ) + $cmdArgs;
            }
            foreach($cmdArgs as $arg => $value){
                $cmd .= ' '.$arg.(substr($arg, 0 , 2) == '--' ? '=' : '').$value;
            }
            $cmd .= ' '.$connection['database'];
            
            if(!empty($distribution[TableFilter::ADD])){
                $cmd .= ' '.implode(' ', $distribution[TableFilter::ADD]);
            }
            if(!empty($distribution[TableFilter::IGNORE])){
                $cmd .= ' --ignore-table="'.$connection['database'].'.'.implode('" --ignore-table="'.$connection['database'].'.', $distribution[TableFilter::IGNORE]).'"';
            }
            fwrite(\STDERR, "cmd: $cmd\n");
            $res = proc_open($cmd, array(1 => $outputProc), $pipes);
            //var_dump(proc_close($res));
        }
        //Предусмотреть случай, когда таблицы вообще в дамп не попадают (например, только дамп хранимых процедур)
    }
}