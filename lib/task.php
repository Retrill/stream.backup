<?php
namespace Webformat\StreamBackup;

Class Task{
	const TASK_DIR = 'tasks';
	
	protected $taskName;
	
	public function __construct($taskName){
		$this->taskName = $taskName;
	}
	
	public function execute(){
		if(($error = $this->getErrors()) !== false){
			echo $error."\n";
			return false;
		}
	}
	
	protected function getTaskDir(){
		return Utils::getBasedir().self::TASK_DIR.DIRECTORY_SEPARATOR.$this->taskName;
	}
	protected function getErrors(){
		$taskDir = $this->getTaskDir();
		if(!is_dir($taskDir)){
			return 'Task dir "'.$taskDir.'" doesn\'t exist!';
		}
		return false;
	}
}