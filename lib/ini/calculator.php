<?php
namespace Webformat\StreamBackup\Ini;
use \Webformat\StreamBackup\Utils;

class Calculator{
    public static function calc($iniPattern, $stringToApply){
        global $logArchive;

    	if(empty($iniPattern)){
            if(is_numeric($iniPattern)){return 0;}
            return '';
        }
        //if(!(bool)preg_match('#^{([^:]+):(.*)}$#i', trim($iniPattern), $matches)){return $iniPattern;}

        /*
            ��������� ���� ���������� �� ��, ����� ��������� ����� ���� {cmdType:cmdExpression} �� ������, ��� ������ cmdExpression
            ���� ����� ����������� �������� ������. ����������� ������������� ������ ������ cmdExpression ��� ������, �����
            ������������ ����������� � ����������� ������ �� �����������. ��������:
            $string = 'some text {regex:#\{#} values, date: {php:return date("Y-m-d");}'
            ���� ������ regex'� �������� ������ �� ���� �� ������������, ���� ������ �� ��������� ��.
            ������ �������� ����������� � ����� {(?2)}, ��. http://php.net/manual/ru/regexp.reference.recursive.php
        */
        $calcResult = preg_replace_callback('#{([\w\d]+):(((?<=\\\)[{}]+|[^{}]+|(?<!\\\){(?2)+}(?!\\\))+)}#i', function($matches)use($stringToApply, $iniPattern){
            $type = strtolower(trim($matches[1]));
            $patternToCalc = trim($matches[2]);
            if(!$stringToApply /*&& !in_array($type, array('php', 'subfolder'))*/){
                //Utils::report('Empty string to apply pattern "'.$iniPattern.'". Ignore');
                switch($type){
                    case 'php': break;
                    case 'getvar': break;
                    case 'subfolder':
                        if((bool)preg_match('/^([^,]+),([^,]+)$/', $patternToCalc, $parts)){
                            
                            $patternToCalc = $parts[2];
                            $stringToApply = $parts[1];
                            break;
                        }
                        break;
                    default: return $matches[0];
                }
            }
            
            $calcMethod = 'calculate'.$type; 

            if(!method_exists(__CLASS__, $calcMethod)){
                Utils::report('Unexpected calculation type "'.$type.'" with value "'.$patternToCalc.'". Ignore.');
                return $stringToApply;
            }
            
            if($error = self::{$calcMethod}($patternToCalc, $stringToApply, $result)){
                Utils::report('Parse error for pattern '.$iniPattern.': '.$error);
                return false;
            }
            return $result;
        }, trim($iniPattern));
        return $calcResult;
    }
    
    protected static function calculateSubfolder($partNumber, $path, &$result){
        $path = trim($path, DIRECTORY_SEPARATOR);
        $pathArray = explode(DIRECTORY_SEPARATOR, $path);
        
        $partNumber = (int)$partNumber;
        //$result = false;
        if(!(bool)$partNumber){
            return 'Numeration of subfolders should start from 1 (or -1), zero values are not supported!';
        }
        $partNumber = ($partNumber > 0) ? ($partNumber - 1) : (count($pathArray) + $partNumber);
        if(!isset($pathArray[$partNumber])){
            return 'Index "'.$partNumber.'" is out of range for the path "'.$path.'"'; 
        }
        $result = $pathArray[$partNumber];
        return false;
    }
    protected static function calculateRegEx($pattern, $string, &$result){
        $result = false;
        $returnTpl = '$1';
        $regexSymbol = substr($pattern, 0, 1);
        $undefinedIndices = array();

        if(preg_match('#^(.*\\'.$regexSymbol.')\s*,\s*(\$.*)$#', $pattern, $matches)){
            $pattern = $matches[1];
            $returnTpl = $matches[2];
        }

        if(!preg_match($pattern, $string, $matches)){
            return ('String "'.$string.'" doesn\'t match the pattern "'.$pattern.'"');
        }
        $result = preg_replace_callback('#\$(\d+)#', function($itemMatches)use($matches, &$undefinedIndices){
            $index = $itemMatches[1];
            if(isset($matches[$index])){return $matches[$index];}
            $undefinedIndices[] = $index;
            return $itemMatches[0];
        }, $returnTpl);
        if($undefinedIndices){
            return 'Indices "'.implode(', ', $undefinedIndices).'" does not exist';
        }
        return false;
    }
    
    protected static function calculatePhp($code, $string = false, &$result){
        $code = trim($code);
        $lastSymbol = substr($code, -1);
        if(!in_array($lastSymbol, array(';', '}'))){
            $code .= ';';
        }
        $result = eval($code);
        if(empty($result)){
            if(strpos($code, 'return') === false){Utils::report('Eval warning: no "return" statement in code "'.substr($code, 0, 70).'"');}
            $result = '';
        }
        return false;
    }
    protected static function calculateGetVar($pattern, $string = false, &$result){
        $parts = explode(':', $pattern);
        if(count($parts) != 2){return 'Parse error for pattern "'.$pattern.'"';}
        $parts = array_map('trim', $parts);
        $filePath = $parts[0];
        if(!file_exists($filePath)){return ('File "'.$filePath.'" does not exist!');}
        $variable = preg_replace('/\[([^\[\]]+)\]/', '["$1"]', $parts[1]);
        
        $cmd = '$streamBackupResult = include "'.$filePath.'"; ';
        if(strpos($variable, '#return#') !== false){
            $variable = str_replace('#return#', '$streamBackupResult', $variable);
        }
        $cmd .= 'echo '.$variable.';';
        $cmd = 'php -r \''.$cmd.'\'';
        $result = shell_exec($cmd);
    }
}