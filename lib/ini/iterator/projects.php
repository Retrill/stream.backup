<?php
namespace Webformat\StreamBackup\Ini\Iterator;
use \Webformat\StreamBackup\Utils;

Class Projects extends \Webformat\StreamBackup\Ini\Iterator{
	protected $exclude;
	protected $taskDir = false;
	protected $iniPrjName;
	//protected $iniSectionReplaced = array();
	
	public function __construct(){
		//parent::__construct($iniSection, $keyToIterate);
		$this->iniPrjName = array('_default_' => 'backup');
	}
	protected function prefillActions(){
		if(isset($this->iniSection['project_name'])){
			if(!is_array($this->iniSection['project_name'])){
				$this->iniPrjName['_default_'] = $this->iniSection['project_name'];
			}else{
				$this->iniPrjName = array_merge($this->iniPrjName, $this->iniSection['project_name']);
			}
		}
    }
	
	protected function addGlobResults($globResults, $iterationKey){
		asort($globResults);
		foreach($globResults as $targetPath){
			if($this->exclude($targetPath)){continue;}
			$prjName = $this->getProjectName($targetPath, $iterationKey);
			$project = new \Webformat\StreamBackup\Project($this->taskDir, $targetPath, $prjName);
			$project->setArchiveName($this->iniSection['archive_name']);
			if(!empty($this->iniSection['aliases'])){
				$project->setAliases($this->iniSection['aliases']);
			}
			$this->stack[] = $project;
		}
	}
	protected function exclude($path){
		if(!isset($this->exclude)){
			$this->exclude = array();
			if(!empty($this->iniSection['exclude'])){
				if(!is_array($this->iniSection['exclude'])){
					$this->exclude = array($this->iniSection['exclude']);
				}else{
					$this->exclude =& $this->iniSection['exclude'];
				}
			}
		}
		foreach($this->exclude as $excludePattern){
			if(fnmatch($excludePattern, $path)){return true;}
		}
		return false;
	}
	
	protected function getProjectName($targetPath, $iterationKey = false){
		($iterationKey !== false) or ($iterationKey = '_default_');

		if(isset($this->iniPrjName[$iterationKey])){
			$namePattern = $this->iniPrjName[$iterationKey];
		}else{
			if(!is_numeric($iterationKey)){
				\Webformat\StreamBackup\Utils::report('Can\'t find project name for "'.$targetPath.'". Choosing default name.');
			}
			$namePattern = $this->iniPrjName['_default_'];
		}
		return $namePattern;
	}
	
	public function setTaskDir($taskDir){
		$this->taskDir = $taskDir;
	}
}