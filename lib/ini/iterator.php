<?php
namespace Webformat\StreamBackup\Ini;
use Webformat\StreamBackup\Utils;

Abstract class Iterator{
	protected $currentPosition = -1;
	public $stack = array();
	protected $iniSection = array();
	
    public function __construct(){}
    
    public function fill($iniSection, $keyToIterate){
    	if(array_key_exists($keyToIterate, $iniSection)){
			$globItems = is_array($iniSection[$keyToIterate]) ? $iniSection[$keyToIterate] : array($iniSection[$keyToIterate]);
			//$this->fillIterator($globItems);
		}else{return;}
    	
    	//if(empty($globItems) || !is_array($globItems)){return;}
    	$this->iniSection = $iniSection;
    	$this->prefillActions();
    	
		foreach($globItems as $patternKey => $globPattern){
			$globPattern = trim($globPattern);
			$flags = 0;
			$flagPattern = '#\|\s*(GLOB_[\w]+)#i';
			if(preg_match_all($flagPattern, $globPattern, $matches)){
				foreach($matches[1] as $globFlag){
					if(defined($globFlag)){
						$flags = $flags | constant($globFlag);
					}else{
						Utils::report('Unexpected GLOB flag "'.$globFlag.'" at pattern "'.$globPattern.'"');
					}
				}
				$globPattern = rtrim(preg_replace($flagPattern, '', $globPattern));
			}
			$globResults = glob($globPattern, $flags);
			//Remove "." and ".." folders:
			$globResults = preg_grep('#\/\.\.?$#', $globResults, PREG_GREP_INVERT);
			$this->addGlobResults($globResults, $patternKey);
		}
    }
    protected function prefillActions(){
		
    }
    protected abstract function addGlobResults($globResults, $iterationKey);
    
    public function next(){
		$this->currentPosition++;
		if(isset($this->stack[$this->currentPosition])){
			return $this->stack[$this->currentPosition];
		}
		return false;
    }
}