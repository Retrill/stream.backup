<?php
namespace Webformat\StreamBackup\Storage;
use \Webformat\StreamBackup\Storage;
//use Webformat\StreamBackup\Utils;

abstract class Concrete extends Storage{
    protected $profile;
    protected $outputStreamId = false;
    protected $maxFileSize = 0; //0 = no limits
    protected $freeBytes = false;
    
    //runtime (profile) vars:
    protected $readLength;
    protected $gateway;
    protected $partitionManager;
    
    protected $fatalError = false;
    protected $fList; //Файлы хранилища: array(file, time, size)
    
    public function __construct($profile){
    	$this->fList = array();
        $this->profile = $profile;
        
        $this->readLength = 1 * 1024 * 1024; //1M
        $this->gateway = $this->getGateway($profile);

    	$this->partitionManager = $this->getPartitionManager($profile);
    }
    
    protected function getPartitionManager($profile){
    	$pathPrefix = empty($profile['destination']) ? '' : $profile['destination'];
    	$numLength = empty($profile['padlength']) ? 0 : (int)$profile['padlength'];
		return new PartitionManager($pathPrefix, '', $numLength);
    }

    public function getOldestFinfo(){
        if(empty($this->fList)){$this->ls();}
    	if(empty($this->fList)){return array('path' => '', 'basename' => '', 'time' => 0, 'size' => 0);}
    	
    	$lastIndex = count($this->fList) - 1;
    	
    	return $this->fList[$lastIndex];
    }
    
    /*public function remove($filepath){
        //Должна также проверяться родительская папка. Если она пустая, то тоже удаляться. Но если это корневая папка хранилища - то нет.
        //Нужно знать размер удалённого файла, чтобы обновить значение $this->freeBytes
    }*/
    
    
    /******************** Методы ниже предполагают какую-то общую логику ****************************/

    public function take(\Webformat\StreamBackup\BidiStreamWrapper $stream){
    	//Если метод возвращает false, хранилище должно помечать себя как занятое? Подумать

    	$this->fatalError = false;
    	//Недурственно было бы отсюда выходить, если поток $streamId пуст, и не создавать пустой файл при connect()
        
        //if(!$this->isFree()){return true;} //$this->freeBytes is initialized now

        //В цикле ниже хранилище пытается максимально выкачать поток (в меру своего свободного пространства), отправляя части поочерёдно
        while($this->isFree() && $stream->hasData() && $this->makePart($stream)){}
        
        /*
        	После цикла у нас либо:
        	- Поток выкачан до конца
        	- Текущее хранилище закончилось (места больше нет)
        	- Возникла ошибка отправки в хранилище
        */
        if($this->fatalError){return false;}
        
        if($stream->hasData()){
			//Тут нам надо понять, пытаться ли освобождать данное хранилище для продолжения записи (или делегировать очистку родителю)
			if($this->parentStorage){
                if($this->parentStorage->isFree()){
                	//Выходим, так как родителю известно о том, что свободное место есть в каком-то другом хранилище - пусть сам решает, куда отправлять дальше
                	return true;
                }
                if(!($freeStorage = $this->parentStorage->removeOldestFile())){
					//Родитель по какой-то причине не смог очистить данные, это в любом случае странно, выходим. Но как ошибочную ситуацию не расцениваем,
					//пусть родитель ещё раз попробует что-нибудь почистить
					return true;
                }
                if($freeStorage === $this){
					//Так уж получилось, что родитель почистил именно нас, и теперь можно продолжать работать - отправлять следующую часть
					return $this->take($stream);
                }
                //А раз мы очутились здесь, то родитель почистил кого-то другого. Пусть сам тогда решает, куда перенаправлять поток
                return true;
            }else{
            	//А тут мы знаем, что сами по себе, и никакого родителя у нас нет. Поэтому и чистить себя пытаемся сами.
                if(!$this->removeOldestFile()){
					return false;
                }
                //Типа тут мы почистили - и можно работать дальше
                return $this->take($stream);
            }
        }
        
        //Если мы дошли до сюда, то наше хранилище вместило весь поток - мы молодцы!
        return true;
    }
    
    protected function makePart(\Webformat\StreamBackup\BidiStreamWrapper $stream){
    	$this->partitionManager->next();
    	$this->gateway->fopen($this->partitionManager->current());
    	$partSize = 0;

    	$readLength = min($this->readLength, ($this->maxFileSize - $partSize));
    	if(!$this->gateway->isUnlimited()){
			$readLength = min($readLength, $this->freeBytes);
    	}
    	while($readLength && ($piece = $stream->read($readLength))){
    		$pieceLength = mb_strlen($piece, '8bit');
    		$partSize += $pieceLength;
    		$this->freeBytes -= $pieceLength;
    		if(!$this->gateway->fwrite($piece)){
    			$this->fatalError = true;
				return false; //Возникла ошибка отправки данных!!!
    		}
			$readLength = min($this->readLength, ($this->maxFileSize - $partSize));
			if(!$this->gateway->isUnlimited()){
				$readLength = min($readLength, $this->freeBytes);
    		}
    	}
    	
    	$this->gateway->fclose();
    	
    	//Здесь уместно вызвать пересчитать свободное пространство (во избежание неточностей)
    	if(!$this->gateway->isUnlimited()){
			$this->freeBytes = $this->gateway->getFreeBytes();
    	}

    	return true;
    }
    
    public function isFree(){
    	if($this->gateway->isUnlimited()){return true;}
    	if($this->freeBytes === false){
			$this->freeBytes = $this->gateway->getFreeBytes();
		}
    	return ($this->freeBytes > 0);
    }
    
    public function removeOldestFile(){
    	if($this->gateway->isUnlimited()){return true;}
    	if(empty($this->fList)){$this->ls();}
    	if(empty($this->fList)){return false;}
    	
    	$lastIndex = count($this->fList) - 1;
    	
    	$finfo = $this->fList[$lastIndex];
    	if(!$this->gateway->remove($finfo['path'])){return false;}
    	//По идее тут ещё надо вызвать метод gateway->removeDirIfEmpty(), вдруг файл был единственным в папке
    	if(!$this->gateway->isUnlimited()){
			$this->freeBytes += $finfo['size'];
    	}
    	
    	unset($this->fList[$lastIndex]);
    	
    	return $this;
    }
    
    protected function ls(){
		$this->flist = $this->gateway->ls();
		usort($this->fList, array($this, 'fsort'));
    }

    public function setFilename($filename){
		$this->partitionManager->setBasename($filename);
    }

    protected function getGateway(&$profile){
		if(empty($profile['gateway'])){
			Utils::report('Error processing output profile "'.$profile['name'].'". Gateway is empty.');
			return false;
		}
		$gatewayClass = __NAMESPACE__.'\\Gateway\\'.$profile['gateway'];
		if(!class_exists($gatewayClass)){
			Utils::report('Class "'.$gatewayClass.'" not found!');
			return false;
		}
		$gateway = new $gatewayClass($profile);
		$minFreeQuota = isset($profile['minfreequota']) ? $profile['minfreequota'] : '10M';
		$gateway->setMinFreeQuota($minFreeQuota);
		return $gateway;
    }
    
    public function setPartitionNumber($value){
    	$value = max($value - 1, 0);
		$this->partitionManager->setIndex($value);
    }
    
    public function getPartitionNumber(){
		return $this->partitionManager->key();
    }
    
    protected function fsort($item1, $item2){
	    if($item1['time'] == $item2['time']){
	    	//В одну секунду могут быть созданы разные части одного архива, сортируем по названиям файлов:
	    	return (($item1['basename'] <= $item2['basename']) ? -1 : 1);
	    }
	    return (($item1['time'] < $item2['time']) ? -1 : 1);
	}
}
