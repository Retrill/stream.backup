<?php
namespace Webformat\StreamBackup\Storage;
use \Webformat\StreamBackup\Storage;
use \Webformat\StreamBackup\Utils;
use \Webformat\StreamBackup\INI;

Class Helper{
    public function getProfiles(&$ini){
        if(empty($ini[INI::CONFIG_EXEC]['output'])){
            Utils::report('No output profileas signed! Project will be ignored.');
            return false;
        }
        $profileMasks = $ini[INI::CONFIG_EXEC]['output'];
        if(!is_array($profileMasks)){$profileMasks = array($profileMasks);}

        $allProfiles = $ini[INI::CONFIG_OUTPUT];
        $allProfiles = array_change_key_case($allProfiles, \CASE_LOWER);
        
        foreach($allProfiles as $pofileName => &$profile){
			$profile = array_change_key_case($profile, \CASE_LOWER);
			$profile['.name'] = $pofileName;
        }
        unset($profile);
        
        $groupedProfiles = $this->groupProfiles($profileMasks, $allProfiles);
        
        if(count($groupedProfiles) < count($profileMasks)){
            Utils::report('Some output profile groups will be ignored (no matches)!');
        }
        
        if(!$groupedProfiles){
            Utils::report('No output profile groups found! Project will be ignored.');
            return false;
        }
        return $groupedProfiles;
    }
    
    protected function groupProfiles($masks, &$profiles){
		$groupedProfiles = array();
		foreach($masks as $mask){
			if(preg_match('/^regex\:\s*(?<regex>\S.*\S)\s*$/i', $mask, $matches)){ //Маска задана в синтаксисе регулярного выражения
				$regex = $matches['regex'];
				if($selectedKeys = preg_grep($regex, array_keys($profiles))){
					$groupedProfiles[] = array_intersect_key($profiles, array_flip($selectedKeys));
				}else{
					Utils::report('No matching profile found (regex "'.$regex.'")');
				}
			}else{
				$selectedProfiles = array();
				foreach($profiles as $sectionName => $sectionData){
					if(fnmatch($mask, $sectionName)){
						$selectedProfiles[$sectionName] = $sectionData;
					}
				}
				if($selectedProfiles){
					$groupedProfiles[] = $selectedProfiles;
				}else{
					Utils::report('No matching profile found (mask "'.$mask.'")');
				}
			}
		}
		return $groupedProfiles;
    }
}
