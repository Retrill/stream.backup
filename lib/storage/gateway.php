<?php
namespace Webformat\StreamBackup\Storage;
use \Webformat\StreamBackup\Storage;
use Webformat\StreamBackup\Utils;

Class Gateway{
    protected $profile;
    protected $streamId = false;
    protected $freeBytes = false;
    protected $profileName;
    
    //runtime (profile) vars:
    protected $minFreeQuota;
    protected $maxFileSize = 0; //0 = no limits
    protected $capacityLimit = 0;
    
    protected $isCorrect = false;
    
    public function __construct($profile){
        //$this->profile = $profile;
        $this->profileName = $profile['.name'];
        $this->minFreeQuota = 10 * 1024 * 1024; //10M - default value
        $this->parseProfile($profile);
    }
    
    public function setMaxFileSize($sizeFormatted){
		$this->maxFileSize = Utils::getCanonicalBytes($sizeFormatted);
    }
    
    public function setMinFreeQuota($formattedBytes){
		$this->minFreeQuota = Utils::getCanonicalBytes($formattedBytes);
    }
    
    public function isCorrect(){
		return $this->isCorrect;
    }
    
    /******************** Эти методы нужно реализовать/подкорректировать в наследниках *************/
    
    abstract protected function initProfile($profile);

    abstract public function fopen($filepath); //По идее, этот метод должен параметром принимать имя файла (№ части)
    abstract public function fwrite($pieceOfData); //Возвращает кол-во записанных байт
    abstract public function fclose(); //=>saveMeta
    
    abstract public function getFreeBytes();
    abstract public function ls(); //array(path, basename, time, size)
    
    abstract public function removeDirIfEmpty($dirpath);
    
    /*
    	Должна также проверяться родительская папка. Если она пустая, то тоже удаляться. Но если это корневая папка хранилища - то нет.
        Нужно знать размер удалённого файла, чтобы обновить значение $this->freeBytes
    */
    abstract public function remove($filepath);
        
    public function isFree(){
		if($this->freeBytes === false){
			$this->freeBytes = $this->getFreeBytes();
		}
		return ($this->freeBytes > 0);
    }
    
    public function isUnlimited(){return false;}
}
