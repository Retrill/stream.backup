<?php
namespace Webformat\StreamBackup\Storage\Gateway;
use Webformat\StreamBackup\Utils;

Class LocalFS extends \Webformat\StreamBackup\Storage\Gateway{ //local filesystem
	protected $targetDir;

	protected function initProfile($profile){
		if(empty($profile['destination']) || !($this->targetDir = $this->prepareDir($profile['destination']))){
			$this->processError('Profile "'.$this->profileName.'" parse error. Destination path is wrong');
			return false;
		}
		$this->capacityLimit = empty($profile['capacityLimit']) ? 0 : Utils::getCanonicalBytes($profile['capacityLimit']);
		
		if(empty($profile['maxFileSize'])){
			$this->maxFileSize = 0;
		}else{
			$this->setMaxFileSize($profile['maxFileSize']);
		}
	}
	
	protected function processError($msg){
		Utils::report($msg);
		$this->isCorrect = false;
	}
	
	protected function prepareDir($path){
        $path = rtrim(str_replace(array('\\', '/'), '/', $path), '/');
        
        if(!file_exists($path)){
            return mkdir($path, 0755, true);
        }else{
            return is_dir($path);
        }
    }
    
    protected function fopen($filepath){
		$targetDir = $this->targetDir.'/'.ltrim(dirname($filepath, '/'));
		if(!$this->prepareDir($targetDir)){
			$this->doParseError('Can\'t create directory "'.$targetDir.'"');
			return false;
		}
		$filepath = rtrim($targetDir, '/').'/'.basename($filepath);
		$this->streamId = fopen($filepath, 'w');
		if($this->streamId === false){
			$this->processError('Write file error!');
			return false;
		}
		return true;
    }
    
    public function fwrite($pieceOfData){
		$writtenBytes = fwrite($this->streamId, $pieceOfData);
		if(!(bool)$writtenBytes){
			$this->processError('Error writing to stream!');
		}
		return $writtenBytes;
    }
    
    public function fclose(){
		$result = (bool)fclose($this->streamId);
		if(!$result){
			$this->processError('Error writing to stream!');
		}
		return $result;
    }
    
    public function getFreeBytes(){
    	$freeSpace = disk_free_space($this->targetDir);
    	if($this->capacityLimit > 0){ //manual limit
			$dirFreeSpace = max(0, $this->capacityLimit - $this->getDirectorySize($this->targetDir));
			$freeSpace = min($freeSpace, $dirFreeSpace);
    	}
		
		return min(0, $freeSpace - $this->minFreeQuota);
    }
    
    public function remove($filepath){
		if(!file_exists($filepath)){return true;}
		if(!is_writable($filepath)){return false;}
		if(is_dir($filepath)){
			return $this->removeDir($filepath);
		}
		return unlink($filepath);
    }
    
	protected function getDirectorySize($path){
	    $bytestotal = 0;
	    $path = realpath($path);
	    if(($path !== false) && ($path != '') && file_exists($path)){
	    	$flags = \FilesystemIterator::SKIP_DOTS | \FilesystemIterator::FOLLOW_SYMLINKS;
	    	$it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path, $flags));
	    	$it->rewind();
	    	while($it->valid()){
			    $bytestotal += $it->getSize();
			    $it->next();
			}
	    }
	    return $bytestotal;
	}
	
	protected function ls(){
		$path = $this->targetDir;
		if(!is_dir($path)){return array();}
		$files = array();
		$flags = \FilesystemIterator::SKIP_DOTS | \FilesystemIterator::FOLLOW_SYMLINKS | \FilesystemIterator::KEY_AS_PATHNAME;
	    $it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path, $flags));
	    $it->rewind();
	    while($it->valid()){
	    	if($it->isFile()){
				$files[] = array(
					'path' => $it->key(),
					'basename' => basename($it->key()),
					'time' => $it->getMTime(),
					'size' => $it->getSize()
				);
	    	}
			$it->next();
		}
		return $files;
	}
	
	public function removeDirIfEmpty($dirpath){
		$canonicalPath = realpath($dirpath);
		//$dirpath = rtrim($dirpath, \DIRECTORY_SEPARATOR);
		if(!(bool)$canonicalPath){
			Utils::report('Cannot canonicalize path "'.$dirpath.'"!');
			return false;
		}
		if(!is_dir($canonicalPath)){
			Utils::report('Path "'.$canonicalPath.'" is not a directory!');
			return false;
		}
		
		$isEmpty = true;
	    $flags = \FilesystemIterator::SKIP_DOTS | \FilesystemIterator::FOLLOW_SYMLINKS;
	    $it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($canonicalPath, $flags));
	    $it->rewind();
	    while($it->valid()){
			if($it->isFile()){
				$isEmpty = false;
				break;
			}
			$it->next();
		}
		
		if($isEmpty){
			return $this->removeDir($canonicalPath);
		}
		return false;
	}
	
	protected function removeDir($path){
		$flags = \FilesystemIterator::SKIP_DOTS /*| \FilesystemIterator::FOLLOW_SYMLINKS*/;
		$it = new \RecursiveIteratorIterator(
		    new \RecursiveDirectoryIterator($path, $flags),
		    \RecursiveIteratorIterator::CHILD_FIRST
		);

		$it->rewind();
	    while($it->valid()){
	    	$itemPath = $it->getRealPath();
	    	if(!is_writable($itemPath)){
				Utils::report('Cannot remove "'.$itemPath.'" (path is not writable)!');
				return false;
	    	}
			$todo = ($it->isDir() ? 'rmdir' : 'unlink');
			if(!($todo($itemPath))){
				Utils::report('Cannot remove "'.$itemPath.'"!');
				return false;
			}
			$it->next();
		}

		return rmdir($path);		
	}
}
