<?php
namespace Webformat\StreamBackup\Storage;
//use \Webformat\StreamBackup\Storage;
//use Webformat\StreamBackup\Utils;

class PartitionManager implements \Iterator{
	protected $partIndex = 0;
	protected $pathPrefix = '';
	protected $basename = '';
	protected $padLength = 3;
	
	public function __construct($pathPrefix, $basename, $padLength){
		$this->pathPrefix = trim($pathPrefix);
		if(mb_strlen($this->pathPrefix, 'UTF-8') > 1){
			$this->pathPrefix = rtrim($this->pathPrefix, '\\/');
		}
		$this->basename = trim($basename);
		$this->padLength = max((int)$padLength, 1);
	}
	
	public function current(){
		$filepath = $this->pathPrefix.'/'.$this->basename;
		if($this->partIndex > 1){
			$filepath .= '.'.str_pad($this->partIndex, $this->padLength, \STR_PAD_LEFT);
		}
		return $filepath;
	}
	
	public function key(){
		return $this->partIndex;
	}
	
	public function next(){
		$this->partIndex++;
	}
	
	public function rewind(){
		$this->partIndex = 0;
	}
	
	public function valid(){
		return (($this->partIndex >= 0) && !empty($this->basename));
	}
	
	public function setBasename($basename){
		$this->basename = trim($basename);
	}
	
	public function setIndex($value){
		$this->partIndex = (int)$value;
	}
}
