<?php
namespace Webformat\StreamBackup\Storage;
use \Webformat\StreamBackup\Storage;
//use Webformat\StreamBackup\Utils;

Class Complex extends Storage{
    protected $childStorages = array(); //Массив объектов
    protected $timelist = array(); //Массив времени создания старейших файлов в хранилищах (в конце - старейший)
    protected $requestedFinfos = array();
    protected $streamToOverflowId = false;

    protected $partitionNumber = 1;
    //protected $parentStorage;
    protected $scannedIndex = 0;
    
    public function __construct($childStorages = array()){
        $this->childStorages = $childStorages;
    }
    
    public function addChildStorage(\Webformat\StreamBackup\Storage $storage){
        $this->childStorages[] = $storage;
    }
    
    public function take(\Webformat\StreamBackup\BidiStreamWrapper $stream){
        while($stream->hasData() && ($freeStorage = $this->getFreeStorage())){
            $freeStorage->setPartitionNumber($this->partitionNumber);
            $freeStorage->take($stream);
            $this->partitionNumber = $freeStorage->getPartitionNumber();
            if($stream->hasData()){
                $this->partitionNumber++;
            }
        }
        //$this->filePartNumber = max(--$filePartToWrite, 1);
        return !$stream->hasData();
    }

    protected function getFreeStorage(){
    	$storagesCnt = count($this->childStorages);
    	/*
    		Перебираем этот цикл только один раз, т.к. если все хранилища
    		заняты, то дальше с ними имеет смысл работать только через удаление старейшего файла
    	*/
        for($this->scannedIndex; $this->scannedIndex < $storagesCnt; $this->scannedIndex++){
            $storage = $this->childStorages[$this->scannedIndex];
            if($storage->isFree()){return $storage;}
        }

        return $this->removeOldestFile();
    }
    
    public function isFree(){
        foreach($this->childStorages as $storage){
            if($storage->isFree()){return true;}
        }
        return false;
    }

    public function close(){
        foreach($this->childStorages as $childStorage){
            $childStorage->close();
        }
        if($this->streamToOverflowId){
            fclose($this->streamToOverflowId);
            $this->streamToOverflowId = false;
        }
    }
    
    /*public function remove($filepath){
        foreach($this->requestedFinfos as $finfo){
            if($finfo['path'] == $filepath){
                $storageIndex = $finfo['storage'];
                $storage = $this->childStorages[$storageIndex];
                return $storage->remove($filepath);
            }
        }
        return false;
    }*/
    
    public function removeOldestFile(){
    	if(empty($this->timelist)){
			$this->initTimeslitst();
    	}
    	if(empty($this->timelist)){return false;}
    	
    	$lastIndex = count($this->timelist) - 1;
    	$oldestStorage = $this->timelist[$lastIndex]['storage'];
    	return $oldestStorage->removeOldestFile();
    }
    
    public function getOldestFinfo(){
    	if(empty($this->timelist)){
			$this->initTimeslitst();
    	}
		if(empty($this->timelist)){return 0;}
    	$lastIndex = count($this->timelist) - 1;
    	return $this->timelist[$lastIndex];
    }
    
    protected function initTimeslitst(){
		$this->timelist = array();
		foreach($this->childStorages as $storageIndex => $storage){
			$oldestFinfo = $storage->getOldestFinfo();
			$oldestFinfo['storage'] = $storage;
    		$this->timelist[] = $oldestFinfo;
        }
        usort($this->timelist, array($this, 'storageSort'));
    }

    public function setFilename($filename){
    	foreach($this->childStorages as $childStorage){
            $childStorage->{__FUNCTION__}($filename);
        }
    }
    
    public function setPartitionNumber($value){
		$this->partitionNumber = (int)$value;
    }
    
    public function getPartitionNumber(){
		return (int)$this->partitionNumber;
    }
    
    protected function storageSort($item1, $item2){
	    if($item1['time'] == $item2['time']){
	    	//В одну секунду могут быть созданы разные части одного архива, первая часть (по имени файла) будет в конце:
	    	return (($item1['basename'] >= $item2['basename']) ? -1 : 1);
	    }
	    return (($item1['time'] > $item2['time']) ? -1 : 1);
	}
}
