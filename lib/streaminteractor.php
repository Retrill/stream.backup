<?php
namespace Webformat\StreamBackup;
//defined('CLI_MODE') || defined('B_PROLOG_INCLUDED') or die('no prolog! '.basename(__FILE__));

class StreamInteractor{
    protected $inputStreamId = false;
    protected $inputDescriptor = false;
    protected $outputStreamId = false;
    protected $outputDescriptor = false;

    public function __construct($mixedInput = false, $mixedOutput = false){ //file descriptor or stream resource
        if(is_resource($mixedInput)){
            $this->inputStreamId = $mixedInput;
        }elseif(is_numeric($mixedInput)){
            $this->inputDescriptor = $mixedInput;
        }
        
        if(is_resource($mixedOutput)){
            $this->outputStreamId = $mixedOutput;
        }elseif(is_numeric($mixedOutput)){
            $this->outputDescriptor = $mixedOutput;
        }
    }
    
    public function setOutputStream($mixedOutput){
		if(is_resource($mixedOutput)){
            $this->outputStreamId = $mixedOutput;
            $this->outputDescriptor = false;
        }elseif(is_numeric($mixedOutput)){
            $this->outputDescriptor = $mixedOutput;
            $this->outputStreamId = false;
        }
    }
    
    public function setInputStream($mixedInput){
		if(is_resource($mixedInput)){
            $this->inputStreamId = $mixedInput;
            $this->inputDescriptor = false;
        }elseif(is_numeric($mixedInput)){
            $this->inputDescriptor = $mixedInput;
            $this->inputStreamId = false;
        }
    }
    
    public function read(){
        if(!$streamId = $this->getStreamId('input')){return false;}
        
        $declaredPortionLength = 0;
        $data = '';
        $lineNumber = 1; while(!feof($streamId)){
            $line = fgets($streamId);
            //echo "\nline: $line\n";
            if($lineNumber == 1){
                if(!preg_match('#^(\d+)(?:\:(.*))?$#U', $line, $matches)){
                    Utils::report('Error parsing meta length!');
                    return false;
                }
                $declaredPortionLength = (int)$matches[1];
                $line = array_key_exists(2, $matches) ? $matches[2] : '';
            }
            if(!$declaredPortionLength){
                $this->closeInput();
                return false;
            }
            $data .= $line;
            if($declaredPortionLength && (mb_strlen($data, '8bit') >= $declaredPortionLength)){break;}
            $lineNumber++;
        }
        //$data = json_decode($data, true);
        $data = unserialize(base64_decode($data));
        return $data;
    }
    
    public function write($data){
        if(!$streamId = $this->getStreamId('output')){return false;}
        
        //$data = \json_encode($data, \JSON_UNESCAPED_UNICODE);
        $data = base64_encode(serialize($data));
        $data = mb_strlen($data, '8bit').':'.$data."\n";
        fwrite($streamId, $data);
    }
    
    protected function getStreamId($type){
        if(!empty($this->{$type.'StreamId'})){return $this->{$type.'StreamId'};}
        switch($this->{$type.'Descriptor'}){
            case 0: $this->{$type.'StreamId'} = \STDIN; break;
            case 1: $this->{$type.'StreamId'} = \STDOUT; break;
            case 2: $this->{$type.'StreamId'} = \STDERR; break;
            default:
                if(!$this->{$type.'StreamId'} = fopen('php://fd/'.$this->{$type.'Descriptor'}, ($type == 'input') ? 'r' : 'w')){
                    Utils::report('Error open pipe with descriptor="'.$this->{$type.'Descriptor'}.'", $type = "'.$type.'"');
                    return false;
                }
        }
        return $this->{$type.'StreamId'};
    }
    
    public function close(){
        $this->closeInput();
        $this->closeOutput();
    }
    
    public function closeInput(){
        if(!empty($this->inputStreamId)){
            fclose($this->inputStreamId);
            $this->inputStreamId = false;
        }
    }
    
    public function closeOutput(){
        if(!empty($this->outputStreamId)){
            fwrite($this->outputStreamId, "0\n");
            fclose($this->outputStreamId);
            $this->outputStreamId = false;
        }
    }
    
    public function __destruct(){$this->close();}
}
