<?php
namespace Webformat\StreamBackup;

Class ReadStreamController{
	protected $readStep = 8192;
	protected $streamInteractor;
	protected $envParams = array();
	
	
	public function __construct($readFileDescriptor = false){
		$opts = $this->getOptions();
		if(!empty($opts['r']) && is_numeric($opts['r'])){
			$this->readStep = (int)$opts['r'];
		}
		
		$this->streamInteractor = new StreamInteractor($readFileDescriptor, false);
	}
	
	protected function getOptions(){
        static $options;
        if(isset($options)){return $options;}
        $optCodes = array(
            'r' => 'stdin-read-step'
        );
        $defaults = array(
            'r' => '8K'
        );
        $options = getopt(
            implode(':', array_keys($optCodes)).':'
        );
        $options = $options + $defaults; //simple inheritance
        $options['r'] = Utils::getCanonicalBytes($options['r']);
        return $options;
    }
    
    public function execute(){
		$this->envParams = $this->receiveParams();
		
		if(!$this->init()){return false;}
		
		while(!feof(\STDIN)){
		    $pieceOfData = fread(\STDIN, $this->readStep);
		    $this->process($pieceOfData);
		}
    }
    
    protected function init(){return true;}
    
    protected function process(&$pieceOfData){}
    
    protected function receiveParams(){
		return $this->streamInteractor->read();
    }
    
    public function __destruct(){
    	$this->streamInteractor->__destruct();
    }
}