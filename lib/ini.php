<?php
namespace Webformat\StreamBackup;
use \Webformat\StreamBackup\Ini\Calculator;

class Ini{
    const TASK_DIR = 'tasks';
    const CONFIG_EXEC = 'execute';
    const CONFIG_PROJECTS = 'projects';
    const CONFIG_DBPROFILES = 'db_profiles';
    const CONFIG_TABLEFILTERS = 'db_table_filters';
    const CONFIG_FILEPROFILES = 'file_profiles';
    const CONFIG_GENERAL = 'general';
    const CONFIG_OUTPUT = 'output';
    const CONFIG_COMPRESSION = 'compression';
    
    public static function parseFromDir($dirPath, $inherit = false, $byName = false){
        static $memo;
        if(!isset($memo)){$memo = array();}
        
        $dirPath = rtrim($dirPath, DIRECTORY_SEPARATOR);
        $locations = $inherit ? self::getPathChain($dirPath) : array($dirPath);
        $resultIniData = array();
        $searchFileName = $byName ? '*'.$byName : '*';
        foreach($locations as $location){
            $locationIniData = array();
            if(array_key_exists($location, $memo)){
                $locationIniData = $memo[$location];
            }else{
                $files = glob($location.DIRECTORY_SEPARATOR.'{,.}'.$searchFileName.'.{ini,INI,ini.php}', GLOB_BRACE | GLOB_NOSORT);
                
                foreach($files as $iniFilePath){
                    if(!$canonicalName = self::getCanonicalName(basename($iniFilePath))){continue;};
                    $locationIniData[$canonicalName] = parse_ini_file($iniFilePath, true);
                }
                $memo[$location] = $locationIniData;
                
            }

            $resultIniData = self::arrayIntegrate($resultIniData, $locationIniData);
        }
        
        static::setAliases($resultIniData);
        return $resultIniData;
    }
    
    protected static function setAliases(&$ini){
		if(empty($ini[static::CONFIG_TABLEFILTERS])){return;}
		if(!isset($ini[static::CONFIG_DBPROFILES])){
			$ini[static::CONFIG_DBPROFILES] = array();
		}
		foreach($ini[static::CONFIG_TABLEFILTERS] as $sectionName => $data){
			if(isset($ini[static::CONFIG_DBPROFILES][$sectionName])){
				throw new \Exception('Ini parse error! Duplicate section "'.$sectionName.'" at configs "'.static::CONFIG_DBPROFILES.'" and "'.static::CONFIG_TABLEFILTERS.'"');
			}
			$ini[static::CONFIG_DBPROFILES][$sectionName] =& $ini[static::CONFIG_TABLEFILTERS][$sectionName];
		}
    }
    
    public static function getPathChain($dirPath){
        $dirPath = rtrim($dirPath, DIRECTORY_SEPARATOR);
        $taskDir = rtrim(Utils::getBasedir().self::TASK_DIR, DIRECTORY_SEPARATOR);
        /*$result = array($taskDir);
        $dirPathPrefix = substr($dirPath, 0, strlen($dirPath));
        if($dirPathPrefix != $taskDir){
            throw new \Exception('Runtime error! Task directory path is not under the backup root folder. Check location of your executable php-script.');
        }
        $chain = trim(substr($dirPath, strlen($taskDir)), '/');
        */
        $currentDir = $dirPath;
        $underBackupRoot = false;
        do{
            $result[] = $currentDir;
            if($currentDir == $taskDir){$underBackupRoot = true;}
            //$currentDir = rtrim(dirname($currentDir), DIRECTORY_SEPARATOR);
        }while(
            ($currentDir != $taskDir) && 
            (($currentDir = rtrim(dirname($currentDir), DIRECTORY_SEPARATOR)) != $result[count($result) - 1])
        );
        if(!$underBackupRoot){
            throw new \Exception('Runtime error! Task directory path "'.$dirPath.'" is not under the backup root folder "'.$taskDir.'". Check location of your executable php-script.');
        }
        asort($result);
        return $result;
    }
    protected static function getCanonicalName($configBasename){
        if(!preg_match('/^[\.\d-]*([\w-_]+)\.ini(\.php|)$/i', $configBasename, $matches)){return false;}
        return trim(strtolower($matches[1]), '-_');
    }
    
    /*
    Рекурсивно объединяет два массива так, что скалярные значения берутся из второго массива,
    а дочерние массивы с числовыми ключами объединяются array_merge'м.
    От array_merge_recursive отличается тем, что скалярные ключи не объединяются в массив,
    от array_merge - поддержкой рекурсивного обхода
    */
    public static function arrayIntegrate($m1, $m2){
	    if(array_key_exists('inherit', $m2) && is_numeric(trim($m2['inherit'])) && !(bool)(int)trim($m2['inherit'])){
	        unset($m2['inherit']);
	        return $m2;
	    }
        if(empty($m1)){return $m2;}
	    $r = $m1;
	    foreach($m1 as $key => $value){
	        if(!array_key_exists($key, $m2)){continue;}
	        if(is_array($value) xor is_array($m2[$key])){
	            throw new \Exception('Runtime error! Unexpected merging of array and non-array values for key "'.$key.'"');
	        }
	        if(!is_array($value)){
	            $r[$key] = $m2[$key];
	            continue;
	        }
	        
            if(array_key_exists('inherit', $m2[$key])){
                $inherit = is_numeric(trim($m2[$key]['inherit'])) && (bool)(int)trim($m2[$key]['inherit']);
            }else{
                $inherit = true;
            }
	        if(self::arrayIsMulti($value) || self::arrayIsMulti($m2[$key])){
	            $r[$key] = self::{__FUNCTION__}($value, $m2[$key]);
            }else{
	            $r[$key] = $inherit ? array_merge($value, $m2[$key]) : $m2[$key];
	        }
	        
	    }
        //Дополняем результат ключами из $m2, которых нет в $m1
        $r = array_merge($r, array_diff_key($m2, $m1));
	    return $r;
	}
	protected static function arrayIsMulti($m){
	    foreach ($m as &$v) {
	        if (is_array($v)) return true;
	    }
	    return false;
	}
	
	public static function doReplaces(&$ini, $replaces){
		$defaultReplaces = array(
			'#date#' => date('Y-m-d'),
			'#datetime#' => date('Y-m-d_H-i-s'),
		);
		$replaces = array_merge($replaces, $defaultReplaces);
		if(is_array($ini)){
			foreach($ini as $key => &$value){
				self::{__FUNCTION__}($value, $replaces);
			}
		}else{
			$ini = str_ireplace(array_keys($replaces), array_values($replaces), $ini);
		}
	}
	public static function doCalculations(&$ini){
		if(is_array($ini)){
			foreach($ini as $key => &$value){
				self::{__FUNCTION__}($value);
			}
		}else{
			$ini = Calculator::calc($ini, false);
		}
	}
    public static function doExtend(&$ini){
        if(!is_array($ini)){return;}
        foreach($ini as $key => &$child){
            if(!is_array($child)){continue;}
            if(isset($child['extend'])){
                self::checkCycles($ini, $key);
                self::applyExtendRecursive($child, $ini);
            }
            self::{__FUNCTION__}($child);
        }
    }
    protected static function checkCycles(&$parentNode, $childKey, $level = 0){
        static $ancestors;
        $ancestors = array_slice($ancestors, 0, $level);
        $ancestors[$level] = $childKey;
        
        $child =& $parentNode[$childKey];
        if(empty($child['extend'])){return true;}
        if(!is_array($child['extend'])){throw new \Exception('"extend" ini variable should be an array! Section "'.$childKey.'"');}
        
        foreach($child['extend'] as $extendKey){
            if(!array_key_exists($extendKey, $parentNode)){throw new \Exception('Unexpected ini node "'.$extendKey.'" to extend in section "'.$childKey.'"!');}
            if(in_array($extendKey, $ancestors)){
                $ancestors[] = $extendKey;
                throw new \Exception('Ini extend cycle: '.implode(' > ', $ancestors));
            }
            self::{__FUNCTION__}($parentNode, $extendKey, $level + 1);
        }
    }
    protected static function applyExtendRecursive(&$node, &$parent){
        if(empty($node['extend']) || !is_array($node['extend'])){return $node;}
        $extendSection = $node['extend'];
        foreach($extendSection as $extendKey){
            $node = self::arrayIntegrate(self::{__FUNCTION__}($parent[$extendKey], $parent), $node);
        }
    }
}