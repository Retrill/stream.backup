<?php
namespace Webformat\StreamBackup;
use \Webformat\StreamBackup\Utils;

Class TableFilter{
    const STRUCTURE = 'structure';
    const DATA = 'data';
    
    const ADD = 'add';
    const IGNORE = 'ignore';
    //const ALLTABLES = '*';
    
    
    protected $connection;
    protected $patterns;
    
    public function __construct(){
        
    }
    public function setConnection($connection){
        $this->connection = $connection;
    }
    public function setTablePatterns($tablePatterns){
        if(!is_array($tablePatterns)){$tablePatterns = array($tablePatterns);}
        $this->patterns = $this->preparePatterns($tablePatterns);
    }
    public function getResult(){
        $result = $this->getResultProto();
        if(!$allTables = $this->getAllTables()){return false;}
        if(empty($this->patterns)){
            Utils::report('table patterns for database "'.$this->connection['database'].'" is empty. Dump all tables.');
            unset($result[static::STRUCTURE]);
            //$result[static::DATA][static::ADD][static::ALLTABLES] = true;
            return $result;
        }
        $tableInstructions = $this->getTableInstructions($this->patterns, $allTables);
        foreach($tableInstructions as $tableName => $instructionBits){
            if($instructionBits === 0){
                //$result[static::IGNORE][] = $tableName;
                continue;
            }
            
            if($instructionBits & bindec('01') > 0){ //save data
                $result[static::DATA][static::ADD][] = $tableName;
            }else{ //save structure only
                $result[static::STRUCTURE][static::ADD][] = $tableName;
            }
        }
        $this->optimizeResult($result, $allTables);
        return $result;
    }
    protected function optimizeResult(&$result, &$allTables){
        $allTablesCount = count($allTables);
        foreach($result as $type => $distribution){
            if(empty($distribution[static::ADD])){
                unset($result[$type]);
                continue;
            }
            $countAdd = count($distribution[static::ADD]);
            if($countAdd == $allTablesCount){
                $result[$type][static::ADD] = array(); //dump all tables
            }else{
                $addPortion = $countAdd / $allTablesCount;
                if($addPortion > 0.8){
                    $result[$type][static::IGNORE] = array_diff($allTables, $distribution[static::ADD]);
                    $result[$type][static::ADD] = array();
                }
            }
            
        }
    }
    protected function getAllTables(){
        $cmd = 'echo "SHOW TABLES;" | mysql -u'.$this->connection['user'].' -p'.$this->connection['password'].' -h'.$this->connection['host'].' '.$this->connection['database'];
        $cmd .= ' 2>/dev/null';
        exec($cmd, $tables, $status);
        if($status !== 0){
            Utils::report('Get tables from database "'.$this->connection['database'].'" ('.$this->connection['user'].'@'.$this->connection['host'].') failed! Check user/password/host variables.');
            return array();
        }
        unset($tables[0]); //remove column name
        return $tables;
    }
    protected function getResultProto(){
        return array(
            static::STRUCTURE => array(
            	static::ADD => array(),
            	static::IGNORE => array()
            ),
            static::DATA => array(
            	static::ADD => array(),
            	static::IGNORE => array()
            )
        );
    }
    protected function getTableInstructions($patterns, &$tables){
        $tableFlags = array_fill_keys($tables, 0); //exclude every table by default
        foreach($patterns as $patternData){
            $pattern = $patternData['pattern'];
            $flags = $patternData['flags'];
            $matchedTables = array_filter($tables, function($table) use($pattern){
                return fnmatch($pattern, $table);
            });
            
            foreach($matchedTables as $tableName){
                if(strpos($flags, 'a') !== false){ //include mode
                    $tableFlags[$tableName] = $tableFlags[$tableName] | $this->getBitsByFlags($flags);
                }else{ //exclude mode
                    $tableFlags[$tableName] = $tableFlags[$tableName] & $this->getBitsByFlags($flags);
                }
            }
        }
        return $tableFlags;
    }
    protected function preparePatterns(&$tablePatterns){
        $result = array();
        foreach($tablePatterns as $pattern){
            $pattern = trim($pattern, ' :');
            $flags = 'asd';
            if((bool)preg_match('#^(.+)\:(.+)$#', $pattern, $matches)){
                $flags = trim($matches[1]);
                $pattern = trim($matches[2]);
            }
            $flags = str_replace('f', 'sd', $flags);
            switch($flags){
                case 'a': $flags = 'asd'; break;
                case 'e': $flags = 'esd'; break;
            }
            $result[] = array(
                'pattern' => $pattern,
                'flags' => $flags,
            );
        }
        //Utils::report('table patterns: '.var_export($result, true));
        return $result;
    }
    protected function getBitsByFlags($flags){
        if(strpos($flags, 'a') !== false){
            $bits = 0;
            if(strpos($flags, 's') !== false){$bits += 2;}
            if(strpos($flags, 'd') !== false){$bits += 1;}
        }else{
            $bits = 3;
            if(strpos($flags, 's') !== false){$bits -= 2;}
            if(strpos($flags, 'd') !== false){$bits -= 1;}
        }
        return $bits;
    }
}