<?php
namespace Webformat\StreamBackup;
use \Webformat\StreamBackup\Utils;

Class FileDumper{
    protected $project;
    protected $ini;
    protected $outputProcess;
    protected $outputProcessNoFilters;
    protected $filtersProcPointer = false;
    protected $binPrefix;
    
    public function __construct(Project $project, $outputProcess){
        $this->project = $project;
        $this->ini = $project->getIni();
        $this->outputProcess = $outputProcess;
        $this->outputProcessNoFilters = $outputProcess;
        $this->binPrefix = Utils::getBasedir().'bin'.\DIRECTORY_SEPARATOR;
    }
    
    protected function openFiltersProc(){
        if($this->filtersProcPointer){return;}
        $cmd = 'php "'.$this->binPrefix.'nullfilter.php"';
        $this->filtersProcPointer = proc_open($cmd, array(0 => array('pipe', 'r'), 1 => $this->outputProcessNoFilters, 2 => \STDERR), $pipes);
        if(isset($this->outputProcess)){unset($this->outputProcess);}
        $this->outputProcess = $pipes[0]; //child StdIn
    }
    
    protected function closeFiltersProc(){
        if(!$this->filtersProcPointer){return;}
        proc_close($this->filtersProcPointer);
        $this->filtersProcPointer = false;
        
        $cmd = 'php -d mbstring.func_overload=0 "'.$this->binPrefix.'tar.php" -F'; //send closing TAR file headers
        Utils::report('Tarball-closing-headers are ready to send');
        $procPointer = proc_open($cmd, array(1 => $this->outputProcessNoFilters), $pipes);
        proc_close($procPointer);
    }
    
    public function __invoke(){
        if(empty($this->ini[Ini::CONFIG_EXEC]['file_profile'])){
            Utils::report('No files selected to backup');
            return false;
        }
        if(!is_array($this->ini[Ini::CONFIG_EXEC]['file_profile'])){
            $this->ini[Ini::CONFIG_EXEC]['file_profile'] = array($this->ini[Ini::CONFIG_EXEC]['file_profile']);
        }
        
        if(count($this->ini[Ini::CONFIG_EXEC]['file_profile']) > 1){
            $this->openFiltersProc();
        }
        
        foreach($this->ini[Ini::CONFIG_EXEC]['file_profile'] as $profileName){
            $this->applyProfile($profileName);
        }
        
        $this->closeFiltersProc();
    }
    
    protected function applyProfile($profileName){
        Utils::report('Приступаем к реализации файл-профиля "'.$profileName.'"');
        if(!array_key_exists($profileName, $this->ini[Ini::CONFIG_FILEPROFILES]) || !is_array($this->ini[Ini::CONFIG_FILEPROFILES][$profileName])){
            Utils::report('File profile "'.$profileName.'" does not exist!');
            return false;
        }
        
        $profile = $this->ini[Ini::CONFIG_FILEPROFILES][$profileName];
        $profile = array_change_key_case($profile, \CASE_LOWER);
        
        if(!$ruleGroups = $this->getProfileKeys($profile)){
            Utils::report('Nothing to do');
            return false;
        }
        
        if(count($ruleGroups) > 1){$this->openFiltersProc();}
        
        foreach($ruleGroups as $set){
            $addRules = array_intersect_key($profile, array_flip($set['include']));
            $excludeRules = array_intersect_key($profile, array_flip($set['exclude']));
            $this->backup($addRules, $excludeRules);
        }

        Utils::report('Профиль реализован');
    }
    
    protected function getProfileKeys(&$profile){
        $result = array();
        $keys = array_keys($profile);
        $addKeys = preg_grep('/^include/', $keys);
        $excludeKeys = preg_grep('/^exclude/', $keys);
        sort($addKeys); //array keys now is renumerated
        sort($excludeKeys);
        
        if(!$addKeys){return array();}
        foreach($addKeys as $index => $groupToAdd){
            $result[$index] = array(
                'include' => array($groupToAdd),
                'exclude' => array()
            );
            if(isset($excludeKeys[$index])){
                $result[$index]['exclude'][] = $excludeKeys[$index];
            }
        }
        //Далее мы начинаем дальнейший перебор элементо по индексу, но уже масива $excludeKeys, если там что-то осталось
        $lastResultIndex = $index;
        $maxIndex = count($excludeKeys) - 1;
        if($maxIndex > $lastResultIndex){
            Utils::report("Следующие exclude-группы не нашли своей include-пары:\n".
            implode(', ', array_slice($excludeKeys, $lastResultIndex + 1)."\n".
            ' Они будут добавлены к include-группе '.implode(', ', $result[$lastResultIndex]['include'])));
        }
        
        for(++$index; $index <= $maxIndex; $index++){
            //Будем дописывать оставшиеся исключения к последней известной нам Add-группе
            $result[$lastResultIndex]['exclude'][] = $excludeKeys[$index];
        }
        
        return $result;
    }
    
    protected function backup($addRules, $excludeRules){
        $cmd = 'php -d mbstring.func_overload=0 "'.$this->binPrefix.'tar.php"';
        $projectRoot = $this->project->getProjectRoot();
        foreach($addRules as $targets){
            $targets = preg_replace(
                array('#^\.(\/.*)$#', '/^.*$/'), 
                array($projectRoot.'$1', '-s "$0"'), 
                $targets
            );
            $cmd .= ' '.implode(' ', $targets);
        }
        foreach($excludeRules as $targets){
            $targets = preg_replace('/^.*$/', '-e "$0"', $targets);
            $cmd .= ' '.implode(' ', $targets);
        }
        Utils::report("cmd:\n".$cmd);
        $procPointer = proc_open($cmd, array(1 => $this->outputProcess, 2 => \STDERR), $pipes);
        proc_close($procPointer);
    }
}
