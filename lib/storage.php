<?php
namespace Webformat\StreamBackup;
use \Webformat\StreamBackup\Utils;

abstract class Storage{
    protected $parentStorage = false;
    
    abstract public function take(\Webformat\StreamBackup\BidiStreamWrapper $stream); //Должно быть True. Возвращает false, если что-то пошло не так
    abstract public function isFree();
    abstract public function getOldestFinfo();
    abstract public function removeOldestFile();
    abstract public function close();
    //abstract public function remove($filepath);
    
    abstract public function setFilename($filename);
    
    abstract public function setPartitionNumber($value);
    abstract public function getPartitionNumber();
    
    public function setParentStorage($parentStorage){
        $this->parentStorage = $parentStorage;
    }
}
