<?php
namespace Webformat\StreamBackup;

Class BidiStreamWrapper{ //bidi = bidirectional
	protected $streamId;
	protected $buffer = '';
	protected $bufferLength = 0;
	protected $streamFinished = false;

	public function __construct($steamId){
		$this->setStream($steamId);
	}

	public function setStream($steamId){
		$this->streamId = $steamId;
	}

	public function read($length = 8192){
	    if($this->bufferLength <= 0){ //Буфер пуст, отдаём содержимое из потока
	        return fread($this->streamId, $length);
	    }
	    
	    //Тут мы уже знаем, что в буфере что-то есть
	    $bufferPartLength = 0;
	    $bufferPart = $this->getFromBuffer($length, $bufferPartLength);
	    if($bufferPartLength >= $length){
	        return $bufferPart;
	    }
	    
	    //В оставшемся случае нужно добрать контент из потока
	    return ($bufferPart.fread($this->streamId, $length - $bufferPartLength));
	}

	public function getBack($pieceOfData, $length = 0){
	    $length = ($length > 0) ? $length : mb_strlen($pieceOfData, '8bit');
	    $this->buffer .= $pieceOfData;
	    $this->bufferLength += $length;
	}
    
    /**
    * Возвращает контент из буфера, уменьшая его на соответствущую длину
    * 
    * @param int $supposedLength Запрашиваемая длина содержимого
    * @param int $realLength Длина содержимого, которое было возвращено методом
    * @return string Содержимое из буфера
    */
    protected function getFromBuffer($supposedLength, &$realLength){
    	$realLength = 0;
        if($this->bufferLength <= 0){ //Буфер пуст, возвращаем пустую строку
            $this->bufferLength = 0;
            $realLength = 0;
            return '';
        }
        if($supposedLength >= $this->bufferLength){ //Содержимое буферу нужно вернуть полностью и обнулить
            $realLength = $this->bufferLength;
            $this->bufferLength = 0;
            $piece = $this->buffer;
            $this->buffer = '';
            return $piece;
        }
        
        //Буфер больше, чем затребовано. Нужно вернуть кусочек из начала буфера
        $piece = mb_substr($this->buffer, 0, $supposedLength, '8bit');
        $realLength = $supposedLength;
        $this->buffer = mb_substr($this->buffer, $supposedLength, mb_strlen($this->buffer, '8bit'), '8bit');
        $this->bufferLength -= $supposedLength;
        return $piece;
    }
    
    public function hasData(){
		if($this->bufferLength){return true;}
		if($this->streamFinished){return false;}
		$nextByte = fread($this->streamId, 1);
        if($nextByte === false){
        	$this->streamFinished = true;
        	return false;
        }
        $this->getBack($nextByte, 1);
        return true;
    }
}
