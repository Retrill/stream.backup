<?php
$phar = new PharData(STDOUT);
$phar->buildFromDirectory('/home/bitrix/phpTar/data');

$phar->buildFromIterator(
    new \ArrayIterator(
        array(
            'folder/except.php' => '/home/bitrix/phpTar/zipStream/Exception.php',
            //'dat' => new \RecursiveDirectoryIterator('/home/bitrix/phpTar/data')
        )
    )
);