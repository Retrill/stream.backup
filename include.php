<?php
namespace Webformat\StreamBackup;

Class Utils{

    public static function getBasedir(){
        static $basedir;
        if(isset($basedir)){return $basedir;}
        
        $scriptPath = (substr($_SERVER['SCRIPT_FILENAME'], 0, 1) == DIRECTORY_SEPARATOR) ? $_SERVER['SCRIPT_FILENAME'] : (rtrim(getcwd().DIRECTORY_SEPARATOR).$_SERVER['SCRIPT_FILENAME']);
        
        $basedir = rtrim(dirname(dirname($scriptPath)), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
        return $basedir;
    }
    
    public static function enableAutoload(){
        spl_autoload_register(array(self, 'autoLoad'), true, true);
    }
    
    protected static function autoLoad($classNameFull){
        $classNameFull = str_replace(array('\\', '/'), ':', $classNameFull);

        $processOnlyPrefix = array('webformat:streambackup:', 'webformat:', 'splitbrain:');
        $removePrefix = array('webformat:streambackup:', 'webformat:');
        
        $found = false;
        foreach($processOnlyPrefix as $prefix){
            if(strtolower(substr($classNameFull, 0, strlen($prefix))) == $prefix){
                $found = true;
                break;
            }
        }
        if(!$found){return false;}

        if(in_array($prefix, $removePrefix)){
            $classNameFull = substr($classNameFull, strlen($prefix));
        }
        
        $nameParts = explode(':', $classNameFull);

        $resultPath = self::getBasedir().'lib'.DIRECTORY_SEPARATOR;
        $lastIndex = count($nameParts) - 1;

        foreach($nameParts as $index => $partName){
            if($index < $lastIndex){
                $targetList = glob($resultPath.'*', GLOB_ONLYDIR);
            }else{
                $targetList = glob($resultPath.'*');
            }
            
            if(!$targetList){return false;}
            
            $found = false;
            foreach($targetList as $realTarget){
                $regEx = ($index < $lastIndex) ? '#^'.$partName.'$#i' : '#^'.$partName.'\.php$#i';
                if(preg_match($regEx, basename($realTarget))){
                    $found = true;
                    $resultPath .= basename($realTarget).DIRECTORY_SEPARATOR;
                    break;
                }
            }
            if(!$found){return false;}
        }
        $resultPath = rtrim($resultPath, DIRECTORY_SEPARATOR);
        if(file_exists($resultPath)){
            include($resultPath);
            return true;
        }

        return false;
    }
    
    public static function basicCheck(){
        if (version_compare(\PHP_VERSION, '5.3.0', '<')) {
            throw new \Exception('PHP 5.3.0+ supported only. Your PHP version is '.PHP_VERSION);
        }
        
        /*if($value = (int)ini_get('mbstring.func_overload')){
            throw new \Exception('Mbstring.func_overload value should be "0", "'.$value.'" given!');
        }*/
        
        $value = mb_internal_encoding();
        if(strtolower($value) != 'utf-8'){
            throw new \Exception('mbstring.internal_encoding should be "UTF-8", "'.$value.'" given!');
        }
    }
    public static function report($msg){fwrite(STDERR, $msg."\n");}
    
    public static function debug($var, $label = '', $showTime = false){
        ob_start();
            var_dump($var);
            $var = ob_get_contents();
        ob_end_clean();
        
        $var = ($showTime ? '['.date('Y-m-d H:i:s').'] ' : '').$var;
        
        $backTrace = debug_backtrace();
        $file = $backTrace[0]['file'];
        if(strpos($file, $_SERVER['DOCUMENT_ROOT']) === 0){
            $file = substr($file, strlen($_SERVER['DOCUMENT_ROOT']));
        }
        $var .= '{'.$file.':'.$backTrace[0]['line'].'}';
        if($label){
            $label = "\e[104m$label:\e[0m"; //see https://misc.flogisoft.com/bash/tip_colors_and_formatting
            static::stderr($label);
        }
        static::stderr($var."\n");
    }
    
    protected static function stderr($str){
        if (defined("STDERR")) {
            fwrite(\STDERR, $str."\n");
        }
	}

    public static function getCanonicalBytes($value){
        $value = str_replace(array(',', ' '), array('.', ''), ($value));
        if(!strlen($value)){return 0;}
        if(!preg_match('#^([\d\.]+)([bkmgt\%]+)?$#i', $value, $matches)){
            throw new \Exception('Unexpected measure format "'.$value.'"!');
        }
        if(!array_key_exists(2, $matches)){
            return (float)$value;
        }
        $measureUnit = strtoupper($matches[2]);
        $value = (float)$matches[1];

        switch($measureUnit){
            case 'T': $value *= 1024;
            case 'G': $value *= 1024;
            case 'M': $value *= 1024;
            case 'K': $value *= 1024;
            case 'B': break;
            default: throw new \Exception('Unexpected measure unit "'.$measureUnit.'"!');
        }
        return $value;
    }
    
    public static function formatBytes($bytes, $minUnit = 'B'){
        if(!is_numeric($bytes)){return $bytes;}
        //if(strpos($bytes, '%') !== false){return $bytes;}
        $bytes = round($bytes);
        if(!$bytes){return 0;}
        $minUnit = strtoupper($minUnit);
        $units = array('B' => 1);
        $units['K'] = 1024 * $units['B'];
        $units['M'] = 1024 * $units['K'];
        $units['G'] = 1024 * $units['M'];
        $units['T'] = 1024 * $units['G'];
        $units = array_reverse($units);
        foreach($units as $notation => $weight){
            if(($bytes % $weight == 0) || ($notation == $minUnit)){
                return (round($bytes / $weight).($notation == 'B' ? '' : $notation));
            }
        }
    }
}

Utils::enableAutoload();