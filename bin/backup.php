<?php
namespace Webformat\StreamBackup;

try{
    $scriptPath = (substr($_SERVER['SCRIPT_FILENAME'], 0, 1) == DIRECTORY_SEPARATOR) ? $_SERVER['SCRIPT_FILENAME'] : (rtrim(getcwd().'/').$_SERVER['SCRIPT_FILENAME']);
    $workDir = rtrim(dirname(dirname($scriptPath)), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
    include($workDir.'include.php');

    Utils::basicCheck();

    $options = Options::get();
    if(empty($options['task'])){
        die('Nothing to do. Use "--task" or "-t" options'."\n");
    }

    foreach($options['task'] as $taskName){
        Utils::report('Starting task "'.$taskName.'"');
        $project = new Project($workDir.'tasks'.DIRECTORY_SEPARATOR.$taskName, '', '');
        $project->execute();
    }
}catch(\Throwable $er){
   // Executed only in PHP 7, will not match in PHP 5
   Utils::report($er->getMessage());
}catch(\Exception $er){
   // Executed only in PHP 5, will not be reached in PHP 7
   Utils::report($er->getMessage());
}
