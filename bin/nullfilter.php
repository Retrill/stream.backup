<?php
/*
    ������ ���� ���������� ����� ���� tar-������ � ��� ����������� ���� ������ ������ NULL-������ �� 512 ���� ������� ��.
    ��� ��������� - tarball ������ �������� ����������� ���� �, �������������, ����� ���� ������� ������� � �������
*/
$stdIn = fopen('php://stdin', 'rb');
$stdOut = fopen('php://stdout', 'wb');
$chain = array();
$emptyChain = false;
while(!feof($stdIn)){
    $block = fread($stdIn, 512);
    $emptyBlock = trim($block, "\0") == '';
    if($emptyBlock && $emptyChain){continue;}
    if(!$emptyBlock){$emptyChain = false;}
    $chain[] = array(
        'data' => $block,
        'empty' => $emptyBlock
    );
    
    $chainCount = count($chain);
    if(($chainCount >= 2) && (array_sum(array_column($chain, 'empty')) == $chainCount)){
        //��� ����� � ������� - ������
        $emptyChain = true;
        $chain = array();
        continue;
    }
    
    if(!$emptyBlock){
        foreach($chain as $blockInfo){
            fwrite($stdOut, $blockInfo['data']);
        }
        $chain = array();
    }
}
