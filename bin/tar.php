<?php
namespace splitbrain\PHPArchive{
	abstract class Archive{
	    const COMPRESS_AUTO = -1;
	    const COMPRESS_NONE = 0;
	    const COMPRESS_GZIP = 1;
	    const COMPRESS_BZIP = 2;

	    /**
	     * Set the compression level and type
	     *
	     * @param int $level Compression level (0 to 9)
	     * @param int $type  Type of compression to use (use COMPRESS_* constants)
	     * @return mixed
	     */
	    abstract public function setCompression($level = 9, $type = Archive::COMPRESS_AUTO);

	    /**
	     * Open an existing archive file for reading
	     *
	     * @param string $file
	     * @throws ArchiveIOException
	     */
	    abstract public function open($file);

	    /**
	     * Read the contents of an archive
	     *
	     * This function lists the files stored in the archive, and returns an indexed array of FileInfo objects
	     *
	     * The archive is closed afer reading the contents, because rewinding is not possible in bzip2 streams.
	     * Reopen the file with open() again if you want to do additional operations
	     *
	     * @return FileInfo[]
	     */
	    abstract public function contents();

	    /**
	     * Extract an existing archive
	     *
	     * The $strip parameter allows you to strip a certain number of path components from the filenames
	     * found in the archive file, similar to the --strip-components feature of GNU tar. This is triggered when
	     * an integer is passed as $strip.
	     * Alternatively a fixed string prefix may be passed in $strip. If the filename matches this prefix,
	     * the prefix will be stripped. It is recommended to give prefixes with a trailing slash.
	     *
	     * By default this will extract all files found in the archive. You can restrict the output using the $include
	     * and $exclude parameter. Both expect a full regular expression (including delimiters and modifiers). If
	     * $include is set, only files that match this expression will be extracted. Files that match the $exclude
	     * expression will never be extracted. Both parameters can be used in combination. Expressions are matched against
	     * stripped filenames as described above.
	     *
	     * The archive is closed afterwards. Reopen the file with open() again if you want to do additional operations
	     *
	     * @param string     $outdir  the target directory for extracting
	     * @param int|string $strip   either the number of path components or a fixed prefix to strip
	     * @param string     $exclude a regular expression of files to exclude
	     * @param string     $include a regular expression of files to include
	     * @throws ArchiveIOException
	     * @return array
	     */
	    abstract public function extract($outdir, $strip = '', $exclude = '', $include = '');

	    /**
	     * Create a new archive file
	     *
	     * If $file is empty, the archive file will be created in memory
	     *
	     * @param string $file
	     */
	    abstract public function create($file = '');

	    /**
	     * Add a file to the current archive using an existing file in the filesystem
	     *
	     * @param string          $file     path to the original file
	     * @param string|FileInfo $fileinfo either the name to us in archive (string) or a FileInfo oject with all meta data, empty to take from original
	     * @throws ArchiveIOException
	     */
	    abstract public function addFile($file, $fileinfo = '');

	    /**
	     * Add a file to the current archive using the given $data as content
	     *
	     * @param string|FileInfo $fileinfo either the name to us in archive (string) or a FileInfo oject with all meta data
	     * @param string          $data     binary content of the file to add
	     * @throws ArchiveIOException
	     */
	    abstract public function addData($fileinfo, $data);

	    /**
	     * Close the archive, close all file handles
	     *
	     * After a call to this function no more data can be added to the archive, for
	     * read access no reading is allowed anymore
	     */
	    abstract public function close();

	    /**
	     * Returns the created in-memory archive data
	     *
	     * This implicitly calls close() on the Archive
	     */
	    abstract public function getArchive();

	    /**
	     * Save the created in-memory archive data
	     *
	     * Note: It is more memory effective to specify the filename in the create() function and
	     * let the library work on the new file directly.
	     *
	     * @param string $file
	     */
	    abstract public function save($file);

	}
	class ArchiveIOException extends \Exception{}
	class ArchiveIllegalCompressionException extends \Exception{}
	class ArchiveCorruptedException extends \Exception{}
	
	
	/**
	 * Class FileInfo
	 *
	 * stores meta data about a file in an Archive
	 *
	 * @author  Andreas Gohr <andi@splitbrain.org>
	 * @package splitbrain\PHPArchive
	 * @license MIT
	 */
	class FileInfo{
	    protected $isdir = false;
	    protected $path = '';
	    protected $size = 0;
	    protected $csize = 0;
	    protected $mtime = 0;
	    protected $mode = 0664;
	    protected $owner = '';
	    protected $group = '';
	    protected $uid = 0;
	    protected $gid = 0;
	    protected $comment = '';

	    /**
	     * initialize dynamic defaults
	     *
	     * @param string $path The path of the file, can also be set later through setPath()
	     */
	    public function __construct($path = '')
	    {
	        $this->mtime = time();
	        $this->setPath($path);
	    }

	    /**
	     * Factory to build FileInfo from existing file or directory
	     *
	     * @param string $path path to a file on the local file system
	     * @param string $as   optional path to use inside the archive
	     * @throws FileInfoException
	     * @return FileInfo
	     */
	    public static function fromPath($path, $as = '')
	    {
	        clearstatcache(false, $path);

	        if (!file_exists($path)) {
	            throw new FileInfoException("$path does not exist");
	        }

	        $stat = stat($path);
	        $file = new FileInfo();

	        $file->setPath($path);
	        $file->setIsdir(is_dir($path));
	        $file->setMode(fileperms($path));
	        $file->setOwner(fileowner($path));
	        $file->setGroup(filegroup($path));
	        $file->setSize(filesize($path));
	        $file->setUid($stat['uid']);
	        $file->setGid($stat['gid']);
	        $file->setMtime($stat['mtime']);

	        if ($as) {
	            $file->setPath($as);
	        }

	        return $file;
	    }

	    /**
	     * @return int the filesize. always 0 for directories
	     */
	    public function getSize()
	    {
	        if($this->isdir) return 0;
	        return $this->size;
	    }

	    /**
	     * @param int $size
	     */
	    public function setSize($size)
	    {
	        $this->size = $size;
	    }

	    /**
	     * @return int
	     */
	    public function getCompressedSize()
	    {
	        return $this->csize;
	    }

	    /**
	     * @param int $csize
	     */
	    public function setCompressedSize($csize)
	    {
	        $this->csize = $csize;
	    }

	    /**
	     * @return int
	     */
	    public function getMtime()
	    {
	        return $this->mtime;
	    }

	    /**
	     * @param int $mtime
	     */
	    public function setMtime($mtime)
	    {
	        $this->mtime = $mtime;
	    }

	    /**
	     * @return int
	     */
	    public function getGid()
	    {
	        return $this->gid;
	    }

	    /**
	     * @param int $gid
	     */
	    public function setGid($gid)
	    {
	        $this->gid = $gid;
	    }

	    /**
	     * @return int
	     */
	    public function getUid()
	    {
	        return $this->uid;
	    }

	    /**
	     * @param int $uid
	     */
	    public function setUid($uid)
	    {
	        $this->uid = $uid;
	    }

	    /**
	     * @return string
	     */
	    public function getComment()
	    {
	        return $this->comment;
	    }

	    /**
	     * @param string $comment
	     */
	    public function setComment($comment)
	    {
	        $this->comment = $comment;
	    }

	    /**
	     * @return string
	     */
	    public function getGroup()
	    {
	        return $this->group;
	    }

	    /**
	     * @param string $group
	     */
	    public function setGroup($group)
	    {
	        $this->group = $group;
	    }

	    /**
	     * @return boolean
	     */
	    public function getIsdir()
	    {
	        return $this->isdir;
	    }

	    /**
	     * @param boolean $isdir
	     */
	    public function setIsdir($isdir)
	    {
	        // default mode for directories
	        if ($isdir && $this->mode === 0664) {
	            $this->mode = 0775;
	        }
	        $this->isdir = $isdir;
	    }

	    /**
	     * @return int
	     */
	    public function getMode()
	    {
	        return $this->mode;
	    }

	    /**
	     * @param int $mode
	     */
	    public function setMode($mode)
	    {
	        $this->mode = $mode;
	    }

	    /**
	     * @return string
	     */
	    public function getOwner()
	    {
	        return $this->owner;
	    }

	    /**
	     * @param string $owner
	     */
	    public function setOwner($owner)
	    {
	        $this->owner = $owner;
	    }

	    /**
	     * @return string
	     */
	    public function getPath()
	    {
	        return $this->path;
	    }

	    /**
	     * @param string $path
	     */
	    public function setPath($path)
	    {
	        $this->path = $this->cleanPath($path);
	    }

	    /**
	     * Cleans up a path and removes relative parts, also strips leading slashes
	     *
	     * @param string $path
	     * @return string
	     */
	    protected function cleanPath($path)
	    {
	    	$prefix = 0;
	    	if(substr($path, 0, 2) == './'){
				$prefix = './';
	    	}
	        $path    = str_replace('\\', '/', $path);
	        $path    = explode('/', $path);
	        $newpath = array();
	        foreach ($path as $p) {
	            if ($p === '' || $p === '.') {
	                continue;
	            }
	            if ($p === '..') {
	                array_pop($newpath);
	                continue;
	            }
	            array_push($newpath, $p);
	        }
	        return ($prefix.trim(implode('/', $newpath), '/'));
	    }

	    /**
	     * Strip given prefix or number of path segments from the filename
	     *
	     * The $strip parameter allows you to strip a certain number of path components from the filenames
	     * found in the tar file, similar to the --strip-components feature of GNU tar. This is triggered when
	     * an integer is passed as $strip.
	     * Alternatively a fixed string prefix may be passed in $strip. If the filename matches this prefix,
	     * the prefix will be stripped. It is recommended to give prefixes with a trailing slash.
	     *
	     * @param  int|string $strip
	     * @return FileInfo
	     */
	    public function strip($strip)
	    {
	        $filename = $this->getPath();
	        $striplen = strlen($strip);
	        if (is_int($strip)) {
	            // if $strip is an integer we strip this many path components
	            $parts = explode('/', $filename);
	            if (!$this->getIsdir()) {
	                $base = array_pop($parts); // keep filename itself
	            } else {
	                $base = '';
	            }
	            $filename = join('/', array_slice($parts, $strip));
	            if ($base) {
	                $filename .= "/$base";
	            }
	        } else {
	            // if strip is a string, we strip a prefix here
	            if (substr($filename, 0, $striplen) == $strip) {
	                $filename = substr($filename, $striplen);
	            }
	        }

	        $this->setPath($filename);
	    }

	    /**
	     * Does the file match the given include and exclude expressions?
	     *
	     * Exclude rules take precedence over include rules
	     *
	     * @param string $include Regular expression of files to include
	     * @param string $exclude Regular expression of files to exclude
	     * @return bool
	     */
	    public function match($include = '', $exclude = '')
	    {
	        $extract = true;
	        if ($include && !preg_match($include, $this->getPath())) {
	            $extract = false;
	        }
	        if ($exclude && preg_match($exclude, $this->getPath())) {
	            $extract = false;
	        }

	        return $extract;
	    }
	}
	class FileInfoException extends \Exception{}

	/**
	 * Class Tar
	 *
	 * Creates or extracts Tar archives. Supports gz and bzip compression
	 *
	 * Long pathnames (>100 chars) are supported in POSIX ustar and GNU longlink formats.
	 *
	 * @author  Andreas Gohr <andi@splitbrain.org>
	 * @package splitbrain\PHPArchive
	 * @license MIT
	 */
	class Tar extends Archive{
	    protected $file = '';
	    protected $comptype = Archive::COMPRESS_AUTO;
	    protected $complevel = 9;
	    protected $fh;
	    protected $memory = '';
	    protected $closed = true;
	    protected $writeaccess = false;

	    /**
	     * Sets the compression to use
	     *
	     * @param int $level Compression level (0 to 9)
	     * @param int $type  Type of compression to use (use COMPRESS_* constants)
	     * @return mixed
	     */
	    public function setCompression($level = 9, $type = Archive::COMPRESS_AUTO)
	    {
	        $this->compressioncheck($type);
	        $this->comptype  = $type;
	        $this->complevel = $level;
	        if($level == 0) $this->comptype = Archive::COMPRESS_NONE;
	        if($type == Archive::COMPRESS_NONE) $this->complevel = 0;
	    }

	    /**
	     * Open an existing TAR file for reading
	     *
	     * @param string $file
	     * @throws ArchiveIOException
	     */
	    public function open($file)
	    {
	        $this->file = $file;

	        // update compression to mach file
	        if ($this->comptype == Tar::COMPRESS_AUTO) {
	            $this->setCompression($this->complevel, $this->filetype($file));
	        }

	        // open file handles
	        if ($this->comptype === Archive::COMPRESS_GZIP) {
	            $this->fh = @gzopen($this->file, 'rb');
	        } elseif ($this->comptype === Archive::COMPRESS_BZIP) {
	            $this->fh = @bzopen($this->file, 'r');
	        } else {
	            $this->fh = @fopen($this->file, 'rb');
	        }

	        if (!$this->fh) {
	            throw new ArchiveIOException('Could not open file for reading: '.$this->file);
	        }
	        $this->closed = false;
	    }

	    /**
	     * Read the contents of a TAR archive
	     *
	     * This function lists the files stored in the archive
	     *
	     * The archive is closed afer reading the contents, because rewinding is not possible in bzip2 streams.
	     * Reopen the file with open() again if you want to do additional operations
	     *
	     * @throws ArchiveIOException
	     * @returns FileInfo[]
	     */
	    public function contents()
	    {
	        if ($this->closed || !$this->file) {
	            throw new ArchiveIOException('Can not read from a closed archive');
	        }

	        $result = array();
	        while ($read = $this->readbytes(512)) {
	            $header = $this->parseHeader($read);
	            if (!is_array($header)) {
	                continue;
	            }

	            $this->skipbytes(ceil($header['size'] / 512) * 512);
	            $result[] = $this->header2fileinfo($header);
	        }

	        $this->close();
	        return $result;
	    }

	    /**
	     * Extract an existing TAR archive
	     *
	     * The $strip parameter allows you to strip a certain number of path components from the filenames
	     * found in the tar file, similar to the --strip-components feature of GNU tar. This is triggered when
	     * an integer is passed as $strip.
	     * Alternatively a fixed string prefix may be passed in $strip. If the filename matches this prefix,
	     * the prefix will be stripped. It is recommended to give prefixes with a trailing slash.
	     *
	     * By default this will extract all files found in the archive. You can restrict the output using the $include
	     * and $exclude parameter. Both expect a full regular expression (including delimiters and modifiers). If
	     * $include is set only files that match this expression will be extracted. Files that match the $exclude
	     * expression will never be extracted. Both parameters can be used in combination. Expressions are matched against
	     * stripped filenames as described above.
	     *
	     * The archive is closed afer reading the contents, because rewinding is not possible in bzip2 streams.
	     * Reopen the file with open() again if you want to do additional operations
	     *
	     * @param string     $outdir  the target directory for extracting
	     * @param int|string $strip   either the number of path components or a fixed prefix to strip
	     * @param string     $exclude a regular expression of files to exclude
	     * @param string     $include a regular expression of files to include
	     * @throws ArchiveIOException
	     * @return FileInfo[]
	     */
	    public function extract($outdir, $strip = '', $exclude = '', $include = '')
	    {
	        if ($this->closed || !$this->file) {
	            throw new ArchiveIOException('Can not read from a closed archive');
	        }

	        $outdir = rtrim($outdir, '/');
	        @mkdir($outdir, 0777, true);
	        if (!is_dir($outdir)) {
	            throw new ArchiveIOException("Could not create directory '$outdir'");
	        }

	        $extracted = array();
	        while ($dat = $this->readbytes(512)) {
	            // read the file header
	            $header = $this->parseHeader($dat);
	            if (!is_array($header)) {
	                continue;
	            }
	            $fileinfo = $this->header2fileinfo($header);

	            // apply strip rules
	            $fileinfo->strip($strip);

	            // skip unwanted files
	            if (!strlen($fileinfo->getPath()) || !$fileinfo->match($include, $exclude)) {
	                $this->skipbytes(ceil($header['size'] / 512) * 512);
	                continue;
	            }

	            // create output directory
	            $output    = $outdir.'/'.$fileinfo->getPath();
	            $directory = ($fileinfo->getIsdir()) ? $output : dirname($output);
	            @mkdir($directory, 0777, true);

	            // extract data
	            if (!$fileinfo->getIsdir()) {
	                $fp = fopen($output, "wb");
	                if (!$fp) {
	                    throw new ArchiveIOException('Could not open file for writing: '.$output);
	                }

	                $size = floor($header['size'] / 512);
	                for ($i = 0; $i < $size; $i++) {
	                    fwrite($fp, $this->readbytes(512), 512);
	                }
	                if (($header['size'] % 512) != 0) {
	                    fwrite($fp, $this->readbytes(512), $header['size'] % 512);
	                }

	                fclose($fp);
	                touch($output, $fileinfo->getMtime());
	                chmod($output, $fileinfo->getMode());
	            } else {
	                $this->skipbytes(ceil($header['size'] / 512) * 512); // the size is usually 0 for directories
	            }

	            $extracted[] = $fileinfo;
	        }

	        $this->close();
	        return $extracted;
	    }

	    /**
	     * Create a new TAR file
	     *
	     * If $file is empty, the tar file will be created in memory
	     *
	     * @param string $file
	     * @throws ArchiveIOException
	     */
	    public function create($file = '')
	    {
	        $this->file   = $file;
	        $this->memory = '';
	        $this->fh     = 0;

	        if ($this->file) {
	            // determine compression
	            if ($this->comptype == Archive::COMPRESS_AUTO) {
	                $this->setCompression($this->complevel, $this->filetype($file));
	            }

	            if ($this->comptype === Archive::COMPRESS_GZIP) {
	                $this->fh = @gzopen($this->file, 'wb'.$this->complevel);
	            } elseif ($this->comptype === Archive::COMPRESS_BZIP) {
	                $this->fh = @bzopen($this->file, 'w');
	            } else {
	                $this->fh = @fopen($this->file, 'wb');
	            }

	            if (!$this->fh) {
	                throw new ArchiveIOException('Could not open file for writing: '.$this->file);
	            }
	        }
	        $this->writeaccess = true;
	        $this->closed      = false;
	    }

	    /**
	     * Add a file to the current TAR archive using an existing file in the filesystem
	     *
	     * @param string $file path to the original file
	     * @param string|FileInfo $fileinfo either the name to us in archive (string) or a FileInfo oject with all meta data, empty to take from original
	     * @throws ArchiveCorruptedException when the file changes while reading it, the archive will be corrupt and should be deleted
	     * @throws ArchiveIOException there was trouble reading the given file, it was not added
	     */
	    public function addFile($file, $fileinfo = '')
	    {
	        if (is_string($fileinfo)) {
	            $fileinfo = FileInfo::fromPath($file, $fileinfo);
	        }

	        if ($this->closed) {
	            throw new ArchiveIOException('Archive has been closed, files can no longer be added');
	        }

	        $fp = fopen($file, 'rb');
	        if (!$fp) {
	            throw new ArchiveIOException('Could not open file for reading: '.$file);
	        }

	        // create file header
	        $this->writeFileHeader($fileinfo);

	        // write data
	        $read = 0;
	        while (!feof($fp)) {
	            $data = fread($fp, 512);
	            $read += strlen($data);
	            if ($data === false) {
	                break;
	            }
	            if ($data === '') {
	                break;
	            }
	            $packed = pack("a512", $data);
	            $this->writebytes($packed);
	        }
	        fclose($fp);

	        if($read != $fileinfo->getSize()) {
	            $this->close();
	            throw new ArchiveCorruptedException("The size of $file changed while reading, archive corrupted. read $read expected ".$fileinfo->getSize());
	        }
	    }

	    /**
	     * Add a file to the current TAR archive using the given $data as content
	     *
	     * @param string|FileInfo $fileinfo either the name to us in archive (string) or a FileInfo oject with all meta data
	     * @param string          $data     binary content of the file to add
	     * @throws ArchiveIOException
	     */
	    public function addData($fileinfo, $data)
	    {
	        if (is_string($fileinfo)) {
	            $fileinfo = new FileInfo($fileinfo);
	        }

	        if ($this->closed) {
	            throw new ArchiveIOException('Archive has been closed, files can no longer be added');
	        }

	        $len = strlen($data);
	        $fileinfo->setSize($len);
	        $this->writeFileHeader($fileinfo);

	        for ($s = 0; $s < $len; $s += 512) {
	            $this->writebytes(pack("a512", substr($data, $s, 512)));
	        }
	    }

	    /**
	     * Add the closing footer to the archive if in write mode, close all file handles
	     *
	     * After a call to this function no more data can be added to the archive, for
	     * read access no reading is allowed anymore
	     *
	     * "Physically, an archive consists of a series of file entries terminated by an end-of-archive entry, which
	     * consists of two 512 blocks of zero bytes"
	     *
	     * @link http://www.gnu.org/software/tar/manual/html_chapter/tar_8.html#SEC134
	     */
	    public function close()
	    {
	        if ($this->closed) {
	            return;
	        } // we did this already

	        // write footer
	        if ($this->writeaccess) {
	            $this->writebytes(pack("a512", ""));
	            $this->writebytes(pack("a512", ""));
	        }

	        // close file handles
	        if ($this->file) {
	            if ($this->comptype === Archive::COMPRESS_GZIP) {
	                gzclose($this->fh);
	            } elseif ($this->comptype === Archive::COMPRESS_BZIP) {
	                bzclose($this->fh);
	            } else {
	                fclose($this->fh);
	            }

	            $this->file = '';
	            $this->fh   = 0;
	        }

	        $this->writeaccess = false;
	        $this->closed      = true;
	    }

	    /**
	     * Returns the created in-memory archive data
	     *
	     * This implicitly calls close() on the Archive
	     */
	    public function getArchive()
	    {
	        $this->close();

	        if ($this->comptype === Archive::COMPRESS_AUTO) {
	            $this->comptype = Archive::COMPRESS_NONE;
	        }

	        if ($this->comptype === Archive::COMPRESS_GZIP) {
	            return gzcompress($this->memory, $this->complevel);
	        }
	        if ($this->comptype === Archive::COMPRESS_BZIP) {
	            return bzcompress($this->memory);
	        }
	        return $this->memory;
	    }

	    /**
	     * Save the created in-memory archive data
	     *
	     * Note: It more memory effective to specify the filename in the create() function and
	     * let the library work on the new file directly.
	     *
	     * @param string $file
	     * @throws ArchiveIOException
	     */
	    public function save($file)
	    {
	        if ($this->comptype === Archive::COMPRESS_AUTO) {
	            $this->setCompression($this->complevel, $this->filetype($file));
	        }

	        if (!file_put_contents($file, $this->getArchive())) {
	            throw new ArchiveIOException('Could not write to file: '.$file);
	        }
	    }

	    /**
	     * Read from the open file pointer
	     *
	     * @param int $length bytes to read
	     * @return string
	     */
	    protected function readbytes($length)
	    {
	        if ($this->comptype === Archive::COMPRESS_GZIP) {
	            return @gzread($this->fh, $length);
	        } elseif ($this->comptype === Archive::COMPRESS_BZIP) {
	            return @bzread($this->fh, $length);
	        } else {
	            return @fread($this->fh, $length);
	        }
	    }

	    /**
	     * Write to the open filepointer or memory
	     *
	     * @param string $data
	     * @throws ArchiveIOException
	     * @return int number of bytes written
	     */
	    protected function writebytes($data)
	    {
	        if (!$this->file) {
	            $this->memory .= $data;
	            $written = strlen($data);
	        } elseif ($this->comptype === Archive::COMPRESS_GZIP) {
	            $written = @gzwrite($this->fh, $data);
	        } elseif ($this->comptype === Archive::COMPRESS_BZIP) {
	            $written = @bzwrite($this->fh, $data);
	        } else {
	            $written = @fwrite($this->fh, $data);
	        }
	        if ($written === false) {
	            throw new ArchiveIOException('Failed to write to archive stream');
	        }
	        return $written;
	    }

	    /**
	     * Skip forward in the open file pointer
	     *
	     * This is basically a wrapper around seek() (and a workaround for bzip2)
	     *
	     * @param int $bytes seek to this position
	     */
	    function skipbytes($bytes)
	    {
	        if ($this->comptype === Archive::COMPRESS_GZIP) {
	            @gzseek($this->fh, $bytes, SEEK_CUR);
	        } elseif ($this->comptype === Archive::COMPRESS_BZIP) {
	            // there is no seek in bzip2, we simply read on
	            // bzread allows to read a max of 8kb at once
	            while($bytes) {
	                $toread = min(8192, $bytes);
	                @bzread($this->fh, $toread);
	                $bytes -= $toread;
	            }
	        } else {
	            @fseek($this->fh, $bytes, SEEK_CUR);
	        }
	    }

	    /**
	     * Write the given file metat data as header
	     *
	     * @param FileInfo $fileinfo
	     */
	    protected function writeFileHeader(FileInfo $fileinfo)
	    {
	        $this->writeRawFileHeader(
	            $fileinfo->getPath(),
	            $fileinfo->getUid(),
	            $fileinfo->getGid(),
	            $fileinfo->getMode(),
	            $fileinfo->getSize(),
	            $fileinfo->getMtime(),
	            $fileinfo->getIsdir() ? '5' : '0'
	        );
	    }

	    /**
	     * Write a file header to the stream
	     *
	     * @param string $name
	     * @param int    $uid
	     * @param int    $gid
	     * @param int    $perm
	     * @param int    $size
	     * @param int    $mtime
	     * @param string $typeflag Set to '5' for directories
	     */
	    protected function writeRawFileHeader($name, $uid, $gid, $perm, $size, $mtime, $typeflag = '')
	    {
	        // handle filename length restrictions
	        $prefix  = '';
	        $namelen = strlen($name);
	        if ($namelen > 100) {
	            $file = basename($name);
	            $dir  = dirname($name);
	            if (strlen($file) > 100 || strlen($dir) > 155) {
	                // we're still too large, let's use GNU longlink
	                $this->writeRawFileHeader('././@LongLink', 0, 0, 0, $namelen, 0, 'L');
	                for ($s = 0; $s < $namelen; $s += 512) {
	                    $this->writebytes(pack("a512", substr($name, $s, 512)));
	                }
	                $name = substr($name, 0, 100); // cut off name
	            } else {
	                // we're fine when splitting, use POSIX ustar
	                $prefix = $dir;
	                $name   = $file;
	            }
	        }

	        // values are needed in octal
	        $uid   = sprintf("%6s ", decoct($uid));
	        $gid   = sprintf("%6s ", decoct($gid));
	        $perm  = sprintf("%6s ", decoct($perm));
	        $size  = sprintf("%11s ", decoct($size));
	        $mtime = sprintf("%11s", decoct($mtime));

	        $data_first = pack("a100a8a8a8a12A12", $name, $perm, $uid, $gid, $size, $mtime);
	        $data_last  = pack("a1a100a6a2a32a32a8a8a155a12", $typeflag, '', 'ustar', '', '', '', '', '', $prefix, "");

	        for ($i = 0, $chks = 0; $i < 148; $i++) {
	            $chks += ord($data_first[$i]);
	        }

	        for ($i = 156, $chks += 256, $j = 0; $i < 512; $i++, $j++) {
	            $chks += ord($data_last[$j]);
	        }

	        $this->writebytes($data_first);

	        $chks = pack("a8", sprintf("%6s ", decoct($chks)));
	        $this->writebytes($chks.$data_last);
	    }

	    /**
	     * Decode the given tar file header
	     *
	     * @param string $block a 512 byte block containing the header data
	     * @return array|false returns false when this was a null block
	     * @throws ArchiveCorruptedException
	     */
	    protected function parseHeader($block)
	    {
	        if (!$block || strlen($block) != 512) {
	            throw new ArchiveCorruptedException('Unexpected length of header');
	        }

	        // null byte blocks are ignored
	        if(trim($block) === '') return false;

	        for ($i = 0, $chks = 0; $i < 148; $i++) {
	            $chks += ord($block[$i]);
	        }

	        for ($i = 156, $chks += 256; $i < 512; $i++) {
	            $chks += ord($block[$i]);
	        }

	        $header = @unpack(
	            "a100filename/a8perm/a8uid/a8gid/a12size/a12mtime/a8checksum/a1typeflag/a100link/a6magic/a2version/a32uname/a32gname/a8devmajor/a8devminor/a155prefix",
	            $block
	        );
	        if (!$header) {
	            throw new ArchiveCorruptedException('Failed to parse header');
	        }

	        $return['checksum'] = OctDec(trim($header['checksum']));
	        if ($return['checksum'] != $chks) {
	            throw new ArchiveCorruptedException('Header does not match it\'s checksum');
	        }

	        $return['filename'] = trim($header['filename']);
	        $return['perm']     = OctDec(trim($header['perm']));
	        $return['uid']      = OctDec(trim($header['uid']));
	        $return['gid']      = OctDec(trim($header['gid']));
	        $return['size']     = OctDec(trim($header['size']));
	        $return['mtime']    = OctDec(trim($header['mtime']));
	        $return['typeflag'] = $header['typeflag'];
	        $return['link']     = trim($header['link']);
	        $return['uname']    = trim($header['uname']);
	        $return['gname']    = trim($header['gname']);

	        // Handle ustar Posix compliant path prefixes
	        if (trim($header['prefix'])) {
	            $return['filename'] = trim($header['prefix']).'/'.$return['filename'];
	        }

	        // Handle Long-Link entries from GNU Tar
	        if ($return['typeflag'] == 'L') {
	            // following data block(s) is the filename
	            $filename = trim($this->readbytes(ceil($return['size'] / 512) * 512));
	            // next block is the real header
	            $block  = $this->readbytes(512);
	            $return = $this->parseHeader($block);
	            // overwrite the filename
	            $return['filename'] = $filename;
	        }

	        return $return;
	    }

	    /**
	     * Creates a FileInfo object from the given parsed header
	     *
	     * @param $header
	     * @return FileInfo
	     */
	    protected function header2fileinfo($header)
	    {
	        $fileinfo = new FileInfo();
	        $fileinfo->setPath($header['filename']);
	        $fileinfo->setMode($header['perm']);
	        $fileinfo->setUid($header['uid']);
	        $fileinfo->setGid($header['gid']);
	        $fileinfo->setSize($header['size']);
	        $fileinfo->setMtime($header['mtime']);
	        $fileinfo->setOwner($header['uname']);
	        $fileinfo->setGroup($header['gname']);
	        $fileinfo->setIsdir((bool) $header['typeflag']);

	        return $fileinfo;
	    }

	    /**
	     * Checks if the given compression type is available and throws an exception if not
	     *
	     * @param $comptype
	     * @throws ArchiveIllegalCompressionException
	     */
	    protected function compressioncheck($comptype)
	    {
	        if ($comptype === Archive::COMPRESS_GZIP && !function_exists('gzopen')) {
	            throw new ArchiveIllegalCompressionException('No gzip support available');
	        }

	        if ($comptype === Archive::COMPRESS_BZIP && !function_exists('bzopen')) {
	            throw new ArchiveIllegalCompressionException('No bzip2 support available');
	        }
	    }

	    /**
	     * Guesses the wanted compression from the given file
	     *
	     * Uses magic bytes for existing files, the file extension otherwise
	     *
	     * You don't need to call this yourself. It's used when you pass Archive::COMPRESS_AUTO somewhere
	     *
	     * @param string $file
	     * @return int
	     */
	    public function filetype($file)
	    {
	        // for existing files, try to read the magic bytes
	        if(file_exists($file) && is_readable($file) && filesize($file) > 5) {
	            $fh = fopen($file, 'rb');
	            if(!$fh) return false;
	            $magic = fread($fh, 5);
	            fclose($fh);

	            if(strpos($magic, "\x42\x5a") === 0) return Archive::COMPRESS_BZIP;
	            if(strpos($magic, "\x1f\x8b") === 0) return Archive::COMPRESS_GZIP;
	        }

	        // otherwise rely on file name
	        $file = strtolower($file);
	        if (substr($file, -3) == '.gz' || substr($file, -4) == '.tgz') {
	            return Archive::COMPRESS_GZIP;
	        } elseif (substr($file, -4) == '.bz2' || substr($file, -4) == '.tbz') {
	            return Archive::COMPRESS_BZIP;
	        }

	        return Archive::COMPRESS_NONE;
	    }
	}
}

namespace Webformat{
	/*
	Пример использования:

	php -d mbstring.func_overload=0 tar.php -pdata: > test4.tar


	php -d mbstring.func_overload=0 tar.php 
	    --php-target=#stdin#:bitrix/backup/db.sql 
	    --php-target=/home/bitrix/backup/restore.php:#basename# 
	    --php-target=/home/bitrix/ext_www/project_name:/
	    --execute="cd /home/bitrix/ext_www/project_name && ls -a | grep -v \"^\.$\" | grep -v \"^\.\.$\" | tar -T - -c"
	*/

	Class GluTAR{
	    protected $tar;
	    protected $fileInfo;
	    protected $options;
	    
	    public function __construct(){
	        try{
	            $this->tar = new \splitbrain\PHPArchive\Tar();
	            $this->tar->setCompression(0, \splitbrain\PHPArchive\Archive::COMPRESS_NONE);
	            $this->tar->create('php://stdout');
	            $this->fileInfo = new \splitbrain\PHPArchive\FileInfo();
	            
	            $this->options = $this->getOptions();
	        }catch(\Exception $er){
	            $this->report($er->getMessage());
	        }
	    }
	    
	    protected function getOptions(){
	        $defaults = array(
	            'stdin-read-step' => '8K', 
	            'exec-read-step' => '8K', 
	            'stdin-memory-buffer' => '512M',
	        );
	        $optCodes = array(
	            'p' => 'php-target',
	            's' => 'shell-target',
	            'E' => 'execute',
	            'e' => 'exclude',
	            'r' => 'stdin-read-step',
	            'm' => 'stdin-memory-buffer',
	        );
	        $emptyOpts = array(
	            't' => 'test',
	            'n' => 'not-close', //Не записывать закрывающие заголовки (действует, если применяются только php-target)
                'F' => 'force-close'
	        );
	        $scalarFields = array('stdin-read-step', 'stdin-memory-buffer', 'exec-read-step');
	        
	        $options = array_map(function(){return array();}, array_flip($optCodes));
	        $shellOpts = getopt(
	            implode(':', array_keys($optCodes)).':'.implode('', array_keys($emptyOpts)), 
	            array_merge(array_map(function($paramCode){return $paramCode.':';}, array_values($optCodes)), array_values($emptyOpts))
	        );
	        foreach($shellOpts as $propCode => $data){
	            $propCode = isset($optCodes[$propCode]) ? $optCodes[$propCode] : $propCode;
	            $propCode = isset($emptyOpts[$propCode]) ? $emptyOpts[$propCode] : $propCode;

	            if(is_array($data)){
	                $options[$propCode] = array_merge($options[$propCode], $data);
	            }else{
	                $options[$propCode][] = $data;
	            }
	        }
	        foreach($scalarFields as $propCode){
	            if((bool)$options[$propCode]){
	                $options[$propCode] = $options[$propCode][count($options[$propCode]) - 1];
	            }else{
	                unset($options[$propCode]);
	            }
	        }
	        $options = $options + $defaults; //simple inheritance
	        
	        foreach($scalarFields as $propCode){
	            $options[$propCode] = $this->getCanonicalBytes($options[$propCode]);
	        }
	        $options['test'] = array_key_exists('test', $options);
            $options['not-close'] = array_key_exists('not-close', $options);
	        $options['force-close'] = array_key_exists('force-close', $options);
	        $this->testOptions($options);
	        return $options;
	    }
	    
	    protected function testOptions(&$options){
	        $errors = array();
	        foreach($options['shell-target'] as $index => $option){
	            if(!(bool)$option){throw new \Exception('Empty -shell-target option received!');}
	            $paths = explode(':', $option);
	            if(count($paths) > 2){
	                $errors[] = 'There is not correct --shell-target definition. Check a count of colon symbol. Target: '.$option;
	            }
	            
	            $from = trim($paths[0]);
	            if(strtolower($from) == '#stdin#'){
	                $errors[] = 'STDIN can\'t be processed by shell TAR util. Use "--php-target" instead of "--shell-target" option.';
	            }
	            //$to = isset($target[1]) ? trim($target[1]) : '';

	            if(strpos($from, DIRECTORY_SEPARATOR) !== 0){
	                $errors[] = 'You should use only absolute file/dir path in the --shell-target param with value "'.$option.'"';
	            }
	            if(!file_exists($from)){
	                $errors[] = 'Path "'.$from.'" does not exist!';
	            }
	        }
	        if(!empty($options['execute']) && !empty($options['shell-target'])){
	            $errors[] = 'Options "--shell-target" and "--execute" can\'t be used together';
	        }
	        if((bool)$errors){
	            $this->report(implode("\n", $errors), true);
	        }
	    }
	    
	    protected function getCanonicalBytes($value){
	        $value = str_replace(array(',', ' '), array('.', ''), ($value));
	        if(!preg_match('#^([\d\.]+)([A-Za-z]+)$#', $value, $matches)){
	            return (float)$value;
	        }
	        $measureUnit = strtoupper($matches[2]);
	        $value = (float)$matches[1];

	        switch($measureUnit){
                case 'T': $value *= 1024;
	            case 'G': $value *= 1024;
	            case 'M': $value *= 1024;
	            case 'K': $value *= 1024;
	            case 'B': break;
	            default: throw new \Exception('Unexpected measure unit "'.$measureUnit.'"!');
	        }
	        return $value;
	    }
	    
	    public function process(){
            if(!empty($this->options['force-close'])){
                $this->tar->close();
                return;
            }
	        if(empty($this->options['php-target']) && empty($this->options['shell-target'])){
	            $this->report('Nothing to do');
	            return;
	        }
	        try{
	            if($this->options['test']){
	                fwrite(STDERR, "\n===========TEST MODE============\n");
	            }
	            $this->doPhpTargets();
	            $this->doShellTargets();
	            $this->doExec();
	            if($this->options['test']){
	                fwrite(STDERR, "================================\n");
	            }
	        }catch(\Exception $er){
	            $this->report($er->getMessage());
	        }
	        
	    }
	    protected function doShellTargets(){
	        if(empty($this->options['shell-target'])){return;}
	        
	        $parts = array(
	            'plain' => array(),
	            'change' => array(),
	            'transform' => array(),
	        );
	        
	        foreach($this->options['shell-target'] as $index => $target){
	            $target = explode(':', $target);
	            /*if(count($target) > 2){
	                throw new \Exception('There is not correct shell-target definition. Check a count of colon symbol. Target: '.$this->options['shell-target'][$index]);
	            }*/
	            $from = trim($target[0]);
	            /*if(strtolower($from) == '#stdin#'){
	                throw new \Exception('STDIN can\'t be processed by shell TAR util. Use "--php-target" instead of "--shell-target" option.');
	            }*/
	            
	            $to = isset($target[1]) ? trim($target[1]) : $from;
	            //if(strtolower($to) == '#basename#'){$to = basename($from);}
	            $parts = array_merge_recursive($parts, $this->getShellTargetParts($from, $to));
	        }
	        
	        $cmd = 'tar -cf - '.$this->combineShellParts($parts).$this->getExclusions();
	        $this->shellExec($cmd);
	    }
	    protected function doPhpTargets(){
	        if(empty($this->options['php-target'])){return;}
	        foreach($this->options['php-target'] as $index => $target){
	            $target = explode(':', $target);
	            if(count($target) > 2){
	                throw new \Exception('There is not correct php-target definition. Check a count of colon symbol. Target: '.$this->options['php-target'][$index]);
	            }
	            $from = trim($target[0]);
	            $to = isset($target[1]) ? trim($target[1]) : $from;
	            //if(!strlen($to)){$to = basename($from);}
	            if(strtolower($to) == '#basename#'){$to = basename($from);}
	            
	            if(strtolower($from) == '#stdin#'){
	                $this->saveStdIn($to);
	            }else{
	                $this->doPhpTarget($from, $to);
	            }
	            
	        }
	        if(empty($this->options['execute']) && empty($this->options['shell-target']) && !$this->options['not-close']){
	            $this->tar->close();
	        }
	    }
	    protected function doExec(){
	        if(empty($this->options['execute'])){return;}
	        $this->shellExec($this->options['execute']);
	    }
	    
	    protected function getExclusions(){
	        if(empty($this->options['exclude'])){return '';}
	        $exclusions = '';
	        foreach($this->options['exclude'] as $rule){
	            $exclusions .= ' --exclude="'.$rule.'"';
	        }
	        return $exclusions;
	    }
	    protected function getShellTargetParts($from, $to){
	        $from = rtrim($from, '/');
	        $archiveCanonicalTo = './';
	        if((strlen($to, '8bit') > 2) && substr($to, 0, 2) == './'){
	            $to = rtrim(substr($to, 2), '/');
	        }else{
	            $to = trim($to, '/');
	        }
	        $archiveCanonicalTo .= $to;

	        if(!(bool)$from){$from = '/';}
	                
	        $parts = array(
	            'plain' => array(),
	            'change' => array(),
	            'transform' => array(),
	        );
	        
	        $fromBasename = basename($from);
	        $cleanedFrom = ltrim($from ,'/');

	        if($archiveCanonicalTo == './'){
	            $parts['change'][] = '"'.$from.'" .';
	        }elseif((strtolower($to) == '#basename#') || ($to == $fromBasename)){
	            $parts['change'][] = '"'.dirname($from).'" "'.(($from == '/') ? '.' : './').$fromBasename.'"';
	        }else{ //$from & $to are equal or rather different
	            if($from == '/'){
	                $parts['change'][] = '"/" "."';
	            }else{
	                $parts['change'][] = '"/" "./'.$cleanedFrom.'"';
	            }
	            
	            if(($cleanedFrom != $to) && (strtolower($to) != '#basename#')){
	                $parts['transform'][] = array(
	                    'from' => './'.$cleanedFrom,
	                    'to' => $archiveCanonicalTo
	                );
	                foreach($this->options['exclude'] as &$rule){
	                    if(substr($rule, 0, strlen($archiveCanonicalTo)) == $archiveCanonicalTo){
	                        $rule = './'.$cleanedFrom.'/'.ltrim(substr($rule, strlen($archiveCanonicalTo)) ,'/');
	                    }
	                }
	            }
	        }
	        return $parts;
	    }
	    protected function combineShellParts(&$parts){
	        $cmd = ltrim(implode(' ', $parts['plain']));
	        foreach($parts['change'] as $changeTarget){
	            $cmd .= ' -C '.$changeTarget;
	        }
	        
	        $possibleDelimiters = array('#', ',', ':', ';', '-', '_', '!', '=', false);
	        foreach($parts['transform'] as $transformPaths){
	            foreach($possibleDelimiters as $delimiter){
	                if((strpos($transformPaths['from'], $delimiter) === false) && (strpos($transformPaths['to'], $delimiter) === false)){
	                    break;
	                }
	            }
	            if($delimiter === false){
	                throw new \Exception('Can\'t choose correct delimiter for transform regular expression! '.var_export($transformPaths, true));
	            }
	            $transformPaths['from'] = ltrim($transformPaths['from'], DIRECTORY_SEPARATOR);
	            $transformPaths['to'] = ltrim($transformPaths['to'], DIRECTORY_SEPARATOR);
	            $cmd .= ' --transform="s'.$delimiter.'^'.$transformPaths['from'].$delimiter.$transformPaths['to'].$delimiter.'"';
	        }
	        return ltrim($cmd);
	    }
	    
	    protected function shellExec($command){
	        if($this->options['test']){
	            fwrite(STDERR, "Command to run: $command\n");
	            return;
	        }
	        
	        $fRes = popen($command, 'r');
	        while(!feof($fRes)){
	            echo fread($fRes, $this->options['exec-read-step']);
	        }
	        pclose($fRes);
	    }
	    
	    
	    protected function saveStdIn($to){
	        $partNumber = 1;
	        while($data = $this->getStdInPart()){
	            $targetName = ($partNumber == 1 && feof(STDIN)) ? $to : ($to.'.'.str_pad($partNumber, 3, '0', STR_PAD_LEFT));
	            if($this->options['test']){
	                fwrite(STDERR, "Writing ".mb_strlen($data, '8bit')." bytes from STDIN to $targetName.\n");
	            }else{
	                $this->tar->addData($targetName, $data);
	            }
	            
	            $partNumber++;
	        }
	        
	    }
	    protected function getStdInPart(){
	        if(feof(STDIN)){return false;}
	        $data = '';
	        $dataLength = 0;
	        $stepLength = $this->options['stdin-read-step'];
	        while(!feof(STDIN)){
	            if($dataLength + $stepLength > $this->options['stdin-memory-buffer']){
	                $stepLength = $this->options['stdin-memory-buffer'] - $dataLength;
	                if($stepLength <= 1){$stepLength = 1;}
	                $this->report('New step length: '.$stepLength);
	            }
	            /*
	            $data .= fread(STDIN, $stepLength);
	            $dataLength += $stepLength;
	            */
	            $data .= ($piece = fread(STDIN, $stepLength));
	            $dataLength += mb_strlen($piece, '8bit');
	            
	            if($dataLength >= $this->options['stdin-memory-buffer']){
	                return $data;
	            }
	        }
	        return $data;
	    }
	    protected function doPhpTarget($pathFrom, $pathTo){
	        $pathFrom = rtrim($pathFrom, '/');
	        $pathTo = trim($pathTo, '/');
	        if(substr($pathTo, 0, 2) != './'){
				$pathTo = './'.$pathTo;
	        }
	        if($this->options['test']){
	            fwrite(STDERR, "Processing target $pathFrom to $pathTo via php-engine.\n");
	            return;
	        }
	        try{
	            $this->fileInfo = $this->fileInfo->fromPath($pathFrom, $pathTo);
	            if((bool)$pathTo){
	                $this->tar->addFile($pathFrom, $this->fileInfo);
	            }
	            if($this->fileInfo->getIsdir()){
	                $children = glob($pathFrom.'/{,.}*', GLOB_BRACE);
	                foreach($children as $item){
	                    $basename = basename($item);
	                    if(in_array($basename, array('.', '..'))){continue;}
	                    $this->{__FUNCTION__}($item, $pathTo.'/'.$basename);
	                }
	            }
	        }catch(\Exception $er){
	            $this->report($er->getMessage());
	        }
	        /*
	        if(is_file($pathFrom)){
	            $this->fileInfo = $this->fileInfo->fromPath($pathFrom, $pathTo);
	            $this->tar->addFile($pathFrom, $this->fileInfo);
	        }elseif(is_dir($pathFrom)){
	            $pathFrom = rtrim($pathFrom, '/');
	            $pathTo = trim($pathTo, '/');
	            if(strlen($pathTo)){
	                $this->fileInfo = $this->fileInfo->fromPath($pathFrom, $pathTo);
	                $this->tar->addFile($pathFrom, $this->fileInfo);
	            }
	            $children = glob($pathFrom.'/{,.}*', GLOB_BRACE);
	            foreach($children as $item){
	                $basename = basename($item);
	                if(in_array($basename, array('.', '..'))){continue;}
	                $this->{__FUNCTION__}($item, $pathTo.'/'.$basename);
	            }
	        }else{
	            $this->report('Target "'.$pathFrom.'" does not exists!');
	        }
	        */
	    }
	    protected function report($message, $exit = false){
	        fwrite(STDERR, "$message\n");
	        if($exit){exit;}
	    }
	    
	    
	    protected function _doShellTarget(){
	        if(empty($this->options['shell-target'])){return;}
	        throw new \Exception('here');
	        $content = file_get_contents('example.txt');
	        $fileSize = mb_strlen($content, '8bit');

	        $this->tar->init_file_stream_transfer('bitrix/backup/db.sql', $fileSize);
	        $this->tar->stream_file_part($content);
	        $this->tar->complete_file_stream();
	        
	        //$command = 'cd /home/bitrix/phpTar/archiveStream && tar -c archiveStream';
	        $command = 'cd /home/bitrix/phpTar/archiveStream && ls -a | grep -v "^\.$" | grep -v "^\.\.$" | tar -T - -c';
	        //find . -maxdepth 1 -print0 | grep -zv "^\.$" | grep -zv "^\.\.$" | xargs -0 tar -T - -c > ../some.tar
	        //two basic folders in archive (data & archiveStream): tar -cf - -C /home/bitrix/phpTar data -C /home/bitrix/phpTar archiveStream > data.tar
	        //Раскрытие текущей папки + добавление в неё стороннего объекта (basename): ls -a | grep -v "^\.$" | grep -v "^\.\.$" | tar -T - -c -C /home/bitrix/phpTar/archiveStream Archive.php > ../data.tar
	        //Объединить контент двух папок: tar -cf - -C /home/bitrix/phpTar/data . -C /home/bitrix/phpTar/archiveStream . > data.tar
	        //Алиас для файла в архиве: tar -cf - -C /home/bitrix/phpTar data -C /home/bitrix/phpTar archiveStream --transform=s,data/.hidden,data2/subfolder/.visible, > data.tar
	        $fRes = popen($command, 'r');
	        while(!feof($fRes)){
	            echo fread($fRes, 8192);
	        }
	        pclose($fRes);
	        
	        //$this->tar->finish();
	    }
	}

	$tar = new GluTAR();
	$tar->process();
}
