<?php
namespace Webformat;

Class Scaner{
    protected $host;
    protected $token;
    
    public function __construct($apiHost, $apiToken){
        $this->host = $apiHost;
        $this->token = $apiToken;
    }
    
    public function getSize($pathToScan){
        $size = 0;
        $info = $this->getInfo($pathToScan);
        $size = $this->calcItemSize($info);
        return $size;
    }
    
    protected function calcItemSize($item){
        $size = 0;
        switch(strtolower($item['type'])){
            case 'file': $size += $item['size']; break;
            case 'dir':
                if(!array_key_exists('_embedded', $item)){
                    $item = $this->getInfo($item['path']);
                }
                if(empty($item['_embedded']['items'])){return $size;}
                foreach($item['_embedded']['items'] as $childItem){
                    $size += $this->{__FUNCTION__}($childItem); 
                }
                break;
        }
        return $size;
    }
    
    public function addInfo($path, $props){
        $content = array('custom_properties' => $props);
        $content = json_encode($content, \JSON_UNESCAPED_UNICODE);
        $context = array( 
            'http' => array( 
                'method' => 'PATCH', 
                'header' => 'Authorization: OAuth ' . "{$this->token}\r\n".
                            "Content-Type: application/json\r\n",
                'content' => $content
            ) 
        );
        $url = 'https://'.$this->host.'/v1/disk/resources/?path='.urldecode($path);
        $context = stream_context_create($context);
        $result = file_get_contents($url, false, $context);
        if(!$result){
            die('Error during file_get_contents!'."\n");
        }
        $result = json_decode($result, true);

        return $result;
    }
    
    public function getInfo($path){
        $request = array(
            'path' => $path,
            //'fields' => 'name,_embedded,size,type,_embedded.items.name,_embedded.items.size,_embedded.items.type,_embedded.items.path'
        );
        $request = http_build_query($request);
        $context = array( 
            'http' => array( 
                'method' => 'GET', 
                'header' => 'Authorization: OAuth ' . "{$this->token}\r\n",
                //'content' => $request
            ) 
        );
        $url = 'https://'.$this->host.'/v1/disk/resources?'.$request;
        $context = stream_context_create($context);
        $result = file_get_contents($url, false, $context);
        if(!$result){
            die('Error during file_get_contents!'."\n");
        }

        $result = json_decode($result, true);
        return $result;
    }
}

/*
$scaner = new Scaner('cloud-api.yandex.net', 'AQAAAAA...-OtJjTj8knU9...');
$size = $scaner->getSize('/superfolder');
echo 'RESULT SIZE:'."\n";
var_dump($size);
*/

$scaner = new Scaner('cloud-api.yandex.net', 'AQAAA...-UmcXP9XT...');
$info = $scaner->getInfo('app:/');
//$info = $scaner->addInfo('app:/', array('mysize' => null,'size' => 100));
var_dump($info);
