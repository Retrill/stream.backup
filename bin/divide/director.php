<?php
$sourceProc = proc_open('cat /home/bitrix/tmp/to-test/00e44da2aaff924463eb05571b55675f.jpg', array(
	1 => array('pipe', 'w'),
	2 => \STDERR
), $pipes);

$sourceStream = $pipes[1];
unset($pipes);

$childProc1 = proc_open('php /home/bitrix/.stream-backup/bin/divide/child.php', array(
	0 => array('pipe', 'r'),
	1 => array('file', '/dev/null', 'w'),
	2 => \STDERR
), $pipes);
$child1Stdin = $pipes[0];
unset($pipes);

$childProc2 = proc_open('php /home/bitrix/.stream-backup/bin/divide/child.php', array(
	0 => array('pipe', 'r'),
	1 => array('file', '/dev/null', 'w'),
	2 => \STDERR
), $pipes);
$child2Stdin = $pipes[0];
unset($pipes);

$start = microtime(true);
while(!feof($sourceStream)){
	$piece = fread($sourceStream, 1 * 1024);
	fwrite($child1Stdin, $piece);
	fwrite($child2Stdin, $piece);
}

fclose($child1Stdin);
fclose($child2Stdin);
fclose($sourceStream);

proc_close($childProc1);
proc_close($childProc2);
proc_close($sourceProc);

$duration = round(microtime(true) - $start);
echo "duration: $duration s\n";