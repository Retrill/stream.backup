<?php
namespace Webformat\StreamBackup;

Class YandexRestPut{
    protected function getOptions(){
        static $options;
        if(isset($options)){return $options;}
        $optCodes = array(
            'h' => 'host',
            /*'u' => 'user',
            'p' => 'password',*/
            'c' => 'credentials-file',
            'f' => 'filename',
            'r' => 'stdin-read-step',
            'l' => 'limit', //bytes
            'P' => 'pad-length'
        );
        $defaults = array(
            'f' => 'archive', 
            'r' => '8K', 
            'l' => 0,
            'P' => 3
        );
        $options = getopt(
            implode(':', array_keys($optCodes)).':'
        );
        $options = $options + $defaults; //simple inheritance
        $options['l'] = $this->getCanonicalBytes($options['l']);
        $options['r'] = $this->getCanonicalBytes($options['r']);
        return $options;
    }
    protected function getCanonicalBytes($value){
        $value = str_replace(array(',', ' '), array('.', ''), ($value));
        if(!preg_match('#^([\d\.]+)([A-Za-z]+)$#', $value, $matches)){
            return (float)$value;
        }
        $measureUnit = strtoupper($matches[2]);
        $value = (float)$matches[1];

        switch($measureUnit){
            case 'G': $value *= 1024;
            case 'M': $value *= 1024;
            case 'K': $value *= 1024;
            case 'B': break;
            default: throw new \Exception('Unexpected measure unit "'.$measureUnit.'"!');
        }
        return $value;
    }
    public function send(){
        $options = $this->getOptions();
        $stdIn = fopen('php://stdin', 'rb');
        if(!isset($options['u'], $options['p'], $options['h'])){
            $remoteFilePath = $options['f'];
        }else{
            $remoteFilePath = 'ftp://'.$options['u'].':'.$options['p'].'@'.$options['h'].'/'.$options['f'];
        }
        $remoteRes = fopen($remoteFilePath, 'w');
        $overflow = NULL;
        
        $archivePart = 1;
        while($this->sendPart($stdIn, $remoteRes, $options['l'], $overflow)){
            fclose($remoteRes);
            $archivePart++;
            $remotePartName = $remoteFilePath.'.'.str_pad($archivePart, $options['P'], '0', STR_PAD_LEFT);
            $remoteRes = fopen($remotePartName, 'w');
        }
        fclose($remoteRes);
        fclose($stdIn);
    }
    protected function sendPart($from, $to, $maxBytes = 0, &$overflow){
        if(feof($from)){return false;}
        $options = $this->getOptions();
        $readLength = $options['r'];
        $dataLength = 0;
        if(isset($overflow)){
            $dataLength += mb_strlen($overflow, '8bit');
            fwrite($to, $overflow);
            $overflow = NULL;
        }
        
        if(!$maxBytes){
            while(!feof($from)){
                fwrite($to, fread($from, $readLength));
            }
        }else{
            while(!feof($from)){
                if($dataLength >= $maxBytes){
                    if(isset($overflow)){return true;}
                    if(feof($from)){return false;}
                    //if(ftell($from) == $maxBytes){return false;}
                    if($nextByte = fread($from, 1)){
                        $overflow = $nextByte;
                    }else{return false;}
                    return true;
                }else{
                    $piece = fread($from, $readLength);
                    $pieceLength = mb_strlen($piece, '8bit');
                    if($dataLength + $pieceLength > $maxBytes){
                        $pieceLength = $maxBytes - $dataLength;
                        $split = $this->splitBytes($piece, $pieceLength);
                        $dataLength = $maxBytes;
                        $piece = $split[0];
                        $overflow = $split[1];
                    }else{
                        $dataLength += $pieceLength;
                    }
                    fwrite($to, $piece);
                }
            }
        }
        return false;
    }
    protected function splitBytes($data, $length){
        return array(
            mb_substr($data, 0, $length, '8bit'),
            mb_substr($data, $length, NULL, '8bit')
        );
    }
}

$sender = new FtpSender();
$sender->send();
