<?php
namespace Webformat\StreamBackup\Bin;
use \Webformat\StreamBackup\Utils;

$scriptPath = (substr($_SERVER['SCRIPT_FILENAME'], 0, 1) == \DIRECTORY_SEPARATOR) ? $_SERVER['SCRIPT_FILENAME'] : (rtrim(getcwd().'/').$_SERVER['SCRIPT_FILENAME']);
$workDir = rtrim(dirname(dirname($scriptPath)), \DIRECTORY_SEPARATOR).\DIRECTORY_SEPARATOR;
include($workDir.'include.php');

$streamInteractor = new StreamInteractor(3, false);
$params = $streamInteractor->read();
$streamInteractor->__destruct();

//file_put_contents('/home/bitrix/tmp/params.txt', var_export($params, true));

$targetFile = fopen('/home/bitrix/tmp/'.getmypid().'-'.basename($params['filename']), 'w');
$stdIn = fopen('php://stdin', 'rb');
while(!feof($stdIn)){
    $content = fread($stdIn, 8 * 1024);
    fwrite($targetFile, $content);
}
fclose($stdIn);
fclose($targetFile);

Class RemoteStorageController extends \Webformat\StreamBackup\ReadStreamController{
	protected $storage;
	
	protected function init(){
		if(empty($this->envParams['profiles']) || !is_array($this->envParams['profiles'])){
			Utils::report('Empty profiles sent to "'.basename(__FILE__).'" script!');
		}
	}
	
	protected function getConcreteStorage($profile, $profileName){
		if(empty($profile['engine'])){
			Utils::report('No sotrage engine declared (profile "'.$profileName.'")');
		}
	}
    
    protected function process(&$pieceOfData){}
    
    public function __destruct(){
    	$this->streamInteractor->__destruct();
    }
}

$controller = new RemoteStorageController();
$controller->execute();
unset($controller); //вызываем деструктор
