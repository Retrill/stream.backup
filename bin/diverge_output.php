<?php
namespace Webformat\StreamBackup\Bin;
use \Webformat\StreamBackup\Utils;

$scriptPath = (substr($_SERVER['SCRIPT_FILENAME'], 0, 1) == \DIRECTORY_SEPARATOR) ? $_SERVER['SCRIPT_FILENAME'] : (rtrim(getcwd().'/').$_SERVER['SCRIPT_FILENAME']);
$workDir = rtrim(dirname(dirname($scriptPath)), \DIRECTORY_SEPARATOR).\DIRECTORY_SEPARATOR;
include($workDir.'include.php');

Class Diverger extends \Webformat\StreamBackup\ReadStreamController{
	protected $resources = array('processes' => array(), 'pipes' => array());
	
	protected function init(){
		if(empty($this->envParams['profiles']) || !is_array($this->envParams['profiles'])){
			Utils::report('Empty profiles sent to "'.basename(__FILE__).'" script!');
		}
		
		$this->initSubprocesses();
		return true;
    }
    
    protected function initSubprocesses(){
		$cmd = 'php -d mbstring.func_overload=0 '.Utils::getBasedir().'bin/remote_storage.php -r '.$this->readStep;

		foreach($this->envParams['profiles'] as $index => $group){
			//if(isset($pipes)){unset($pipes);}
			$this->resources['processes'][$index] = proc_open($cmd, array(
				0 => array('pipe', 'r'),
				1 => array('file', '/dev/null', 'w'),
				2 => \STDERR,
				3 => array('pipe', 'r') //Сюда мы будем передавать скрипту параметры
			), $pipes);
			$this->resources['pipes'][$index] = $pipes;
			$this->streamInteractor->setOutputStream($pipes[3]);
			$this->streamInteractor->write(array(
				'filename' => $this->envParams['filename'],
				'profiles' => $group
			));
			fclose($pipes[3]);
			unset($pipes);
		}
    }
    
    protected function process(&$pieceOfData){
		foreach($this->resources['pipes'] as $pipes){
			fwrite($pipes[0], $pieceOfData);
		}
    }
    
    public function __destruct(){
    	$this->streamInteractor->__destruct();
    	foreach($this->resources['pipes'] as $pipes){
			fclose($pipes[0]);
			//fclose($pipes[1]);
			//fclose($pipes[2]);
		}

		foreach($this->resources['processes'] as $procId){
			proc_close($procId);
		}
    }
}

$diverger = new Diverger();
$diverger->execute();
unset($diverger); //вызываем деструктор
