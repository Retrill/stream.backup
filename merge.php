<?php

function array_unique_recursive($array)
{
  $result = array_map("unserialize", array_unique(array_map("serialize", $array)));
 
  foreach ($result as $key => $value)
  {
    if ( is_array($value) )
    {
      $result[$key] = array_unique_recursive($value);
    }
  }
 
  return $result;
}

function is_multi($m) {
    foreach ($m as &$v) {
        if (is_array($v)) return true;
    }
    return false;
}

function array_merge2($m1, $m2){
    if(array_key_exists('inherit', $m2) && !(bool)(int)trim($m2['inherit'])){
        unset($m2['inherit']);
        return $m2;
    }
    $r = $m1;
    foreach($m1 as $key => $value){
        if(!array_key_exists($key, $m2)){continue;}
        if(is_array($value) xor is_array($m2[$key])){
            throw new \Exception('Runtime error! Merging of array and non-array value for key "'.$key.'"');
        }
        if(!is_array($value)){
            $r[$key] = $m2[$key];
            continue;
        }
        
        if(is_multi($value) || is_multi($m2[$key])){
            $r[$key] = call_user_func(__FUNCTION__, $value, $m2[$key]);
        }else{
            $r[$key] = array_merge($value, $m2[$key]);
        }
        
    }
    return $r;
}

$m1 = array('filename' => parse_ini_file('/home/bitrix/phpTar/tasks/.file_filters.ini'));
$m2 = array('filename' => parse_ini_file('/home/bitrix/phpTar/tasks/.file_filters2.ini'));

//$r = array_merge_recursive($m1, $m2);
$r = array_merge2($m1, $m2);

$r = array_unique_recursive($r);
/*
$a1 = array('apple', 'banana');
$a2 = array('apricot');
var_dump(array_merge($a1, $a2));
*/
var_dump($r);